angular.module('webapp')
.directive("routeLoadingIndicator",function($rootScope,$timeout){   
    //Directiva para el loading de las vistas
    return {
        restrict: 'E',        
        template: "<div class='jumbotron text-center' ng-if='isRouteLoading'><h1>{{loadingText}} <span><i class='fa fa-refresh fa-spin'></i></span></h1></div>",
        link: function(scope, elem, attrs) {
            scope.loadingText = "Cargando"
            scope.isRouteLoading = false;
            //console.log("Esto esta jalando")
            $rootScope.$on('$routeChangeStart', function() {
                scope.isRouteLoading = true;                                
            });

            $rootScope.$on('$routeChangeSuccess', function() {
                scope.isRouteLoading = false;                                
            });
        }
    }
})
.directive('prueba',function(){
    return {
        restrict:'E',
        template:"<div>Esto es una prueba</div>"
    }
});
