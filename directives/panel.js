angular.module("webapp")
.directive('panel',function(){
    return {
        restrict:'E',
        //template:'panel personalizado',
        transclude:true,        
        scope:{
            title:"@",
            color:"@",
            processing:"="
        },
        templateUrl:'directives/templates/panel.html'
    }
});