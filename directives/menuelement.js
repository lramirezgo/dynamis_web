angular.module('webapp')
.directive("menuelement",function($compile){ 
    return {
        restrict:"E",
        replace:true,
        scope:{
            elemento:'='            
        },        
        template:'<li>'+
                    '<a href="{{elemento.nodo.string1.url}}">'+
                        '<i class="fa {{elemento.nodo.string1.icono}}"></i>'+
                        '<span>{{elemento.nodo.string1.titulo}}</span>'+
                        '<span class="pull-right-container">'+
                            '<small class="label label-primary pull-right {{elemento.marcador.class}}">{{elemento.marcador.texto}}</small>'+
                        '</span>'+
                    '</a>'+                    
                '</li>',
        link:function(scope, element, attrs){
            if(scope.elemento.nodo.tipo == 'menu'){         
                element.addClass('treeview'); //Agrego la clase en caso que sea un submenu
                var sm = element.find('small');
                sm.replaceWith('<i class="fa fa-angle-left pull-right"></i>');
                var laa = element.find('a');                
                angular.forEach(scope.elemento, function(item){
                    laa.after('<ul class="treeview-menu"><menuelement ng-repeat="item in elemento.opciones" elemento="item"></ul>');
                });
                $compile(element.contents())(scope);
            }
        }
    }
});