angular.module('webapp')
    .directive("campoTextoDir", function () {
        return {
            restrict: 'E',
            scope: {
                campoTexto: '=info',
                model: '=model',
                click: '&onClick'
            },
            templateUrl: 'directives/templates/campoTexto.html',
            link: function (scope, element, attrs) {
                scope.campoTexto.txt.password = scope.campoTexto.txt.password == true ? 'password' : 'text';
                scope.campoTexto.txt.disabled = scope.campoTexto.txt.disabled == undefined ? false : scope.campoTexto.txt.disabled;
                scope.campoTexto.txt.foco = scope.campoTexto.txt.foco == undefined ? false : scope.campoTexto.txt.foco;
                if (scope.campoTexto.boton != undefined) {
                    if (scope.campoTexto.boton.encendido) {
                        scope.campoTexto.boton.iconoBoton = scope.campoTexto.boton.iconoBoton == undefined ? 'fa-flash' : scope.campoTexto.boton.iconoBoton;
                        scope.campoTexto.boton.color = scope.campoTexto.boton.color == undefined ? 'default' : scope.campoTexto.boton.color;
                    }
                }
                scope.seleccionar= function(){
                    scope.campoTexto.txt.func()
                }
            }

        };


    });