angular.module('webapp')
.directive("sidemenu",function($rootScope,$timeout){ 
    return {
        restrict:"E",
        replace:true,
        template:'<ul class="sidebar-menu" data-widget="tree" ng-controller="menu as menuc">' +
                    '<li class="header">{{pnavegacion.titulo}}</li>'+
                    '<menuelement ng-repeat="item in pnavegacion.rama.opciones" elemento="item">'+
                  '</ul>'
    }
});