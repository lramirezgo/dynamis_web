angular.module('webapp')
    .directive("tablaDir", function (exportar) {
        return {
            restrict: 'E',
            scope: {
                datos: '=info',
                campos: '=conf'
            },
            templateUrl: 'directives/templates/tabla.html',
            link: function (scope, element, attrs) {
                scope.campos.colapsarsiempre = scope.campos.colapsarsiempre == undefined ? false : scope.campos.colapsarsiempre;
                
                //EXPORTAR
                scope.exportarXL = function () {
                    scope.nombre_archivo = 'Dynamis web'
                    exportar.exportarXLS(scope.nombre_archivo, scope.datos);
                    scope.nombre_archivo = 'Dynamis web';
                }
                // TERMINA EXPORTAR

                ///TABLA
                scope.Math = window.Math;
                scope.pagina_act = 1;
                scope.rest_pagina_act = function () {
                    scope.pagina_act = 1;
                }
                scope.regresar_pag = function () {
                    scope.pagina_act = parseInt(scope.pagina_act) - 1;
                    if (scope.pagina_act == 0) {
                        scope.pagina_act = 1;
                    }
                };

                scope.avanzar_pag = function () {
                    scope.pagina_act = parseInt(scope.pagina_act) + 1;
                    if (scope.pagina_act > Math.ceil(scope.datos.length / 10)) {
                        scope.pagina_act = Math.ceil(scope.datos.length / 10);
                    }
                };

                ///TERMINA TABLA
                scope.seleccionar= function(registro, index){
                    scope.campos.func (registro, index);
                }
                scope.limpiar_busqueda = function(){
                    console.info("entre a limpiar busqueda");
                }
                
            }
        };

    });