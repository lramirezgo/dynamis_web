angular.module('webapp')
    .directive("botonDir", function () {
        return {
            restrict: 'E',
            scope: {
                boton: '=info',
                click: '&onClick'
            },
            templateUrl: 'directives/templates/boton.html',
            link:function(scope, element, attrs){
                scope.boton.color = scope.boton.color == undefined ? 'primary': scope.boton.color;
                scope.boton.tamano = scope.boton.tamano == undefined ? 'flat': scope.boton.tamano;
                scope.boton.texto = scope.boton.texto == undefined ? 'aceptar': scope.boton.texto;
                scope.boton.icono = scope.boton.icono == undefined ? 'fa-check': scope.boton.icono;
                scope.boton.datos = scope.boton.datos == undefined ? {}: scope.boton.datos;
            }
            
        };
    });