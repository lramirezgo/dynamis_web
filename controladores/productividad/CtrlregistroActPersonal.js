angular.module('webapp')
    .controller('CtrlregistroActPersonal', function ($scope, msgBox, PeticionHttp, Notificaciones, $rootScope) {

        $scope.actividad = {};

        $scope.actividad.usuario = $scope.usuario;
        //CONFIGURACIONES
        $scope.txtusuario = {
            txt: {
                placeholder: 'Usuario',
                label: 'Usuario',
                icono: 'fa-user',
                password: false,
                disabled: true
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-flash',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtarea = {
            txt: {
                placeholder: 'Seleccione un area',
                label: 'Area',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: true,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtactividad = {
            txt: {
                placeholder: 'Seleccione una actividad',
                label: 'Actividad',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: true,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtarticulo = {
            txt: {
                placeholder: 'Seleccione un articulo',
                label: 'Articulo',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: true,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.boton_iniciar_actividad = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Iniciar actividad',
            cargando_boton: false,
            icono: 'fa-play',
            datos: {}
        };
        $scope.boton_limpiar = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Iniciar actividad',
            cargando_boton: false,
            icono: 'fa-save',
            datos: {}
        };
        $scope.boton_play = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Iniciar actividad',
            cargando_boton: false,
            icono: 'fa-save',
            datos: {}
        };
        $scope.boton_pausa = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Iniciar actividad',
            cargando_boton: false,
            icono: 'fa-save',
            datos: {}
        };
        $scope.boton_finalizar = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Iniciar actividad',
            cargando_boton: false,
            icono: 'fa-save',
            datos: {}
        };
        $scope.abrir_catalogo_areas = function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de tipo de remision",
                template: "views/msjBox/msjBoxCatalogos.html",
                controlador: "CtrlCatalogos",
                datos: {
                    grp: 'area'
                }
            })
                .then(function (respuesta) {
                    $scope.actividad.area = respuesta.codigo;
                    $scope.actividad.descarea = respuesta.descripcion;
                }, function () {
                });
        };
        $scope.abrir_catalogo_actividades = function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de tipo de remision",
                template: "views/msjBox/msjBoxCatalogos.html",
                controlador: "CtrlCatalogos",
                datos: {
                    grp: 'actividad',
                    subgrp: $scope.actividad.area
                }
            })
                .then(function (respuesta) {
                    $scope.actividad.actividad = respuesta.codigo;
                    $scope.actividad.desactividad = respuesta.descripcion;
                }, function () {

                });
        };
        $scope.abrir_catalogo_articulos = function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de productos",
                template: "views/msjBox/msjBoxCatProductos.html",
                controlador: "CtrlCatProductos",
                datos: ''
            })
                .then(function (respuesta) {
                    $scope.actividad.articulo = respuesta.producto;
                    $scope.actividad.descarticulo = respuesta.descripcion;
                }, function () {
                });
        };
        //END CONFIGURACIONES
    });