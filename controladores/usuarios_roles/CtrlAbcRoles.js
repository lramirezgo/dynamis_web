angular.module('webapp')
    .controller('CtrlAbcRoles', function ($scope, PeticionHttp, msgBox, Notificaciones) {
        $scope.titulo = 'ABC roles';

        $scope.rol = {};

        ///CONFIGURACION DE DOM
        $scope.boton_catalogo = {
            color: 'primary',
            tamano: 'flat',
            texto: 'Abrir catalogo',
            cargando_boton: false,
            icono: 'fa-book',
            datos: {}
        };
        $scope.boton_limpiar = {
            color: 'primary',
            tamano: 'flat',
            texto: 'Limpiar',
            cargando_boton: false,
            icono: 'fa-eraser',
            datos: {}
        };
        $scope.boton_guardar = {
            color: 'primary',
            tamano: 'flat',
            texto: 'Guardar',
            cargando_boton: false,
            icono: 'fa-save',
            datos: {}
        };
        $scope.boton_eliminar = {
            color: 'danger',
            tamano: 'flat',
            texto: 'Eliminar',
            cargando_boton: false,
            icono: 'fa-trash',
            datos: {}
        };
        $scope.tabla_opciones_disp = {
            nocolapsado: [
                {
                    alias: 'OPCION',
                    campo: 'opcion',
                    tamano: '4'
                },
                {
                    alias: 'RELACION',
                    campo: 'relacion',
                    tamano: '4'
                },
                {
                    alias: 'TIPO',
                    campo: 'tipo',
                    tamano: '4'
                }
            ],
            colapsado: ['descripcion', 'relacion', 'tipo'],
            func: function (registro) {
                $scope.rol.opcion = registro;
                $scope.asignar_opcion_rol($scope.rol.opcion.opcion);
            }
        };
        $scope.tabla_opciones_rol = {
            nocolapsado: [
                {
                    alias: 'OPCION',
                    campo: 'opcion',
                    tamano: '4'
                },
                {
                    alias: 'RELACION',
                    campo: 'relacion',
                    tamano: '4'
                },
                {
                    alias: 'TIPO',
                    campo: 'tipo',
                    tamano: '4'
                }
            ],
            colapsado: ['descripcion', 'relacion', 'tipo'],
            func: function (registro) {
                $scope.rol.opcion = registro;
                $scope.eliminar_opcion_rol($scope.rol.opcion.opcion);
            }
        };
        $scope.txtrol = {
            txt: {
                placeholder: 'Rol',
                label: 'Rol',
                icono: 'fa-users',
                password: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-save',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtdescripcion = {
            txt: {
                placeholder: 'Descripcion',
                label: 'Descripcion',
                icono: 'fa-file-text',
                password: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-save',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txttipoopcion = {
            txt: {
                placeholder: 'Seleccione tipo de opcion',
                label: 'Tipo opcion',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: true,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        Notificaciones.banderaNotificacion = true;
        ///CONFIGURACION DE DOM

        $scope.abrir_catalogo = function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de roles",
                template: "views/msjBox/msjBoxCatRoles.html",
                controlador: "CtrlCatRoles",
                datos: ''
            })
                .then(function (respuesta) {
                    $scope.rol = respuesta;
                    $scope.lista_opciones_por_rol = '';
                    $scope.lista_opciones_disp = '';
                }, function () {
                });
        };
        $scope.abrir_catalogo_tipo_opciones = function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de opciones",
                template: "views/msjBox/msjBoxCatalogos.html",
                controlador: "CtrlCatalogos",
                datos: {
                    grp: 'TIPO-OPCION'
                }

            })
                .then(function (respuesta) {
                    $scope.rol.tipo_opcion = respuesta.codigo;
                    $scope.lista_opciones_rol();
                    $scope.opciones_disponibles();
                }, function () {
                });
        };
        $scope.lista_roles = function () {
            $scope.request = {};
            $scope.cargando = true;
            $scope.request.cmdj = "listar roles";
            PeticionHttp.getAll($scope.request)
                .then(function (data) {
                    $scope.lista_roles = data.rows;
                    $scope.cargando = false;

                });
        };
        $scope.lista_tipo_opciones = function () {
            $scope.request = {};
            $scope.cargando_opciones = true;
            $scope.request.cmdj = "listar codigos where grp = 'TIPO-OPCION' and estado = 'A'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_opciones) {
                    $scope.datos_lista_tipo_opciones = data_opciones.rows;
                    $scope.cargando_opciones = false;

                });
        };
        $scope.ver_opciones = function () {
            $scope.lista_opciones_rol();
            $scope.opciones_disponibles();

        };
        $scope.lista_opciones_rol = function () {
            $scope.request = {};
            $scope.cargando_opciones_rol = true;
            $scope.request.cmdj = "listar opciones de rol where rol = '" + $scope.rol.rol + "' and tipo = '" + $scope.rol.tipo_opcion + "'    and sinmenus = 1";
            PeticionHttp.getAll($scope.request)
                .then(function (data_opciones_rol) {
                    $scope.lista_opciones_por_rol = data_opciones_rol.rows;
                    $scope.cargando_opciones_rol = false;

                }, function () {
                    $scope.cargando_opciones_rol = false;
                });
        }
        $scope.opciones_disponibles = function () {
            $scope.request = {};
            $scope.cargando_opciones_disp = true;
            $scope.request.cmdj = "listar opciones no asignadas a rol where rol = '" + $scope.rol.rol + "' and tipo = '" + $scope.rol.tipo_opcion + "' and sinmenus = 1";
            PeticionHttp.getAll($scope.request)
                .then(function (data_opciones_disponibles) {
                    $scope.lista_opciones_disp = data_opciones_disponibles.rows;
                    $scope.cargando_opciones_disp = false;

                }, function () {
                    $scope.cargando_opciones_disp = false;
                });
        };
        $scope.asignar_opcion_rol = function (opcion) {
            $scope.cargando_opciones_disp = true;
            $scope.cargando_opciones_rol = true;
            $scope.request.cmdj = "asignar pantalla a rol where opcion  = '" + opcion + "'  and rol  = '" + $scope.rol.rol + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (result_asignacion) {
                    //Notificaciones.notificacion("CORRECTO", result_asignacion.rows[0].msj, "success", true);
                    $scope.cargando_opciones_disp = false;
                    $scope.cargando_opciones_rol = false;
                    $scope.lista_opciones_rol();
                    $scope.opciones_disponibles();
                });
        };
        $scope.eliminar_opcion_rol = function (opcion) {
            $scope.cargando_opciones_disp = true;
            $scope.cargando_opciones_rol = true;
            $scope.request.cmdj = "eliminar pantalla de rol where opcion  = '" + opcion + "'  and rol  = '" + $scope.rol.rol + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (result_eliminar_asignacion) {
                    //Notificaciones.notificacion("CORRECTO", result_eliminar_asignacion.rows[0].msj, "success", true);
                    $scope.cargando_opciones_disp = false;
                    $scope.cargando_opciones_rol = false;
                    $scope.lista_opciones_rol();
                    $scope.opciones_disponibles();
                });
        };
        $scope.guardar_rol = function () {
            $scope.boton_guardar.cargando_boton = true;
            if ($scope.rol.rol == "" || $scope.rol.rol == undefined || $scope.rol.descripcion == "" || $scope.rol.descripcion == undefined) {
                Notificaciones.notificacion("ERROR", "Debe ingresar un codigo y una descripcion de rol", "error", true);
                $scope.boton_guardar.cargando_boton = false;
                return;
            };
            $scope.request.cmdj = "crear rol where rol  = '" + $scope.rol.rol + "'  and descripcion  = '" + $scope.rol.descripcion + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (result_guardar_rol) {
                    Notificaciones.notificacion("CORRECTO", result_guardar_rol.rows[0].msj, "success", true);
                    $scope.boton_guardar.cargando_boton = false;
                }, function () {
                    $scope.boton_guardar.cargando_boton = false;
                });
        };
        $scope.eliminar_rol = function () {
            msgBox.popUpOk({
                mensaje: "¿Confirma que desea eliminar el rol?",
                titulo: "Confirmacion",
                template: "views/msjBox/msjBoxConfirmacion.html",
                controlador: "CtrlConfirmacion",
                datos: ''
            }).then(function (respuesta) {
                $scope.boton_eliminar.cargando_boton = true;
                $scope.request.cmdj = "eliminar rol  where rol  = '" + $scope.rol.rol + "'";
                PeticionHttp.getAll($scope.request)
                    .then(function (result_eliminar_rol) {
                        Notificaciones.notificacion("CORRECTO", result_eliminar_rol.rows[0].msj, "success", true);
                        $scope.boton_eliminar.cargando_boton = false;
                        $scope.limpiar();
                    }, function () {
                        $scope.boton_eliminar.cargando_boton = false;
                    });
            }, function () {
            });
        };
        $scope.limpiar = function () {
            $scope.rol = {};
            $scope.lista_opciones_por_rol = '';
            $scope.lista_opciones_disp = '';
        };
        $scope.lista_tipo_opciones();


    });