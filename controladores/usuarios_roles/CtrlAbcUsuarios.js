angular.module('webapp')
    .controller('CtrlAbcUsuarios', function ($scope, msgBox, PeticionHttp, Notificaciones, $uibModal, $rootScope, $mdDialog, Upload) {
        $scope.titulo = 'ABC usuarios';
        $scope.request = {};
        $scope.usuario = {};
        var guardar = true;
        $scope.usus_cont = false;

        Notificaciones.banderaNotificacion = true;

        ///CONFIGURACIONES
        $scope.tabla = {
            nocolapsado: [
                {
                    alias: 'ROL',
                    campo: 'rol',
                    tamano: '6'
                },
                {
                    alias: 'DESCRIPCION',
                    campo: 'descripcion',
                    tamano: '6'
                }
            ],
            colapsado: ['rol', 'descripcion'],
            colapsarsiempre: false,
            func: function (registro) {
                $scope.quitar_rol(registro);
            }
        };
        $scope.txtusuario = {
            txt: {
                placeholder: 'Ingrese el ID de usuario',
                label: 'ID usuario',
                icono: 'fa-user',
                password: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-save',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtcontrasena = {
            txt: {
                placeholder: 'Ingrese contraseña',
                label: 'Contraseña',
                icono: 'fa-asterisk',
                password: true
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-save',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtnumempleado = {
            txt: {
                placeholder: 'Ingrese un numero de empleado',
                label: 'Numero de empleado',
                icono: 'fa-file-text',
                password: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'a-barcode',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtnombre = {
            txt: {
                placeholder: 'Ingrese el nombre',
                label: 'Nombre',
                icono: 'fa-ellipsis-h',
                password: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'a-barcode',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtcorreo = {
            txt: {
                placeholder: 'Ingrese correo electronico',
                label: 'Correo',
                icono: 'fa-envelope-square',
                password: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'a-barcode',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtpuesto = {
            txt: {
                placeholder: 'Seleccione puesto',
                label: 'Puesto',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: true,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtrol = {
            txt: {
                placeholder: 'Seleccione rol',
                label: 'Rol',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: true,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.boton_asignar_rol = {
            color: $scope.color_elementos,
            tamano: 'lg',
            texto: 'Asignar',
            cargando_boton: false,
            icono: ' fa-arrow-down',
            datos: {}
        };
        $scope.txtnuevacontrasena = {
            txt: {
                placeholder: 'Ingrese nueva contraseña',
                label: 'Nueva contraseña',
                icono: 'fa-asterisk',
                password: true
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-save',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtconfirmarcontrasena = {
            txt: {
                placeholder: 'Ingrese confirmacion',
                label: 'Confirmacion de contraseña',
                icono: 'fa-asterisk',
                password: true
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-save',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.boton_contrasena = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Guardar contraseña',
            cargando_boton: false,
            icono: 'fa-save',
            datos: {}
        };
        ////CONFIGURACIONES
        $scope.abrir_catalogo = function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de usuarios",
                template: "views/msjBox/msjBoxCatUsuarios.html",
                controlador: "CtrlCatUsuarios",
                datos: ""
            }).then(function (respuesta) {
                $scope.usuario = respuesta;
                guardar = false;
                $scope.usus_cont = true;
                $scope.lista_roles_usuario = '';
                $scope.roles_usuario();

            }, function () {
            });
        };
        $scope.abrir_catalogo_puestos = function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de puestos",
                template: "views/msjBox/msjBoxCatalogos.html",
                controlador: "CtrlCatalogos",
                datos: {
                    grp: 'PUESTO'
                }

            })
                .then(function (respuesta) {
                    $scope.usuario.puesto = respuesta.codigo;
                    $scope.usuario.puesto_desc = respuesta.desc_corta;
                }, function () {
                });
        };
        $scope.abrir_catalogo_roles = function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de roles",
                template: "views/msjBox/msjBoxCatRoles.html",
                controlador: "CtrlCatRoles",
                datos: ''
            })
                .then(function (respuesta) {
                    $scope.usuario.rol = respuesta;
                }, function () {
                });
        };
        $scope.limpiar = function () {
            $scope.usuario = {};
            $scope.lista_roles_usuario = '';
            var guardar = true;
            $scope.usus_cont = false;
        };
        $scope.busqueda_puestos = function () {
            $scope.request = {};
            $scope.request.cmdj = "listar codigos where grp = 'PUESTO'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_busqueda_puesto) {
                    $scope.lista_puestos = data_busqueda_puesto.rows;

                });
        };

        /// ABC USUARIO
        $scope.guardar = function () {
            if (guardar) {
                $scope.guardar_usuario();
            } else {
                $scope.editar_usuario();
            }

        };
        $scope.guardar_usuario = function () {
            if ($scope.usuario.usuario == "" || $scope.usuario.usuario == undefined || $scope.usuario.contrasena == "" || $scope.usuario.contrasena == undefined || $scope.usuario.nombre == "" || $scope.usuario.nombre == undefined || $scope.usuario.puesto == "" || $scope.usuario.puesto == undefined || $scope.usuario.numempleado == "" || $scope.usuario.numempleado == undefined) {
                Notificaciones.notificacion("ERROR", "Debe llenar todos los datos", "error", true);
                return;
            };

            $scope.request.cmdj = "crear usuario where usuario = '" + $scope.usuario.usuario + "'  and password = '" + $scope.usuario.contrasena + "'    and numempleado = '" + $scope.usuario.numempleado + "' and nombre = '" + $scope.usuario.nombre + "' and correo = '" + $scope.usuario.correo + "' and puesto = '" + $scope.usuario.puesto + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_guardar_usuario) {
                    guardar = false;
                    Notificaciones.notificacion("CORRECTO", data_guardar_usuario.rows[0].msj, "success", true);
                });

        };
        $scope.editar_usuario = function () {
            if ($scope.usuario.usuario == "" || $scope.usuario.usuario == undefined || $scope.usuario.contrasena == "" || $scope.usuario.contrasena == undefined || $scope.usuario.nombre == "" || $scope.usuario.nombre == undefined || $scope.usuario.puesto == "" || $scope.usuario.puesto == undefined || $scope.usuario.numempleado == "" || $scope.usuario.numempleado == undefined) {
                Notificaciones.notificacion("ERROR", "Debe llenar todos los datos", "error", true);
                return;
            };
            $scope.request.cmdj = "actualizar usuario where usuario = '" + $scope.usuario.usuario + "' and password = '" + $scope.usuario.contrasena + "'  and numempleado = '" + $scope.usuario.numempleado + "' and nombre = '" + $scope.usuario.nombre + "' and correo = '" + $scope.usuario.correo + "' and puesto = '" + $scope.usuario.puesto + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_editar_usuario) {
                    guardar_componente = false;
                    Notificaciones.notificacion("CORRECTO", data_editar_usuario.rows[0].msj, "success", true);
                });
        };
        $scope.eliminar_usuario = function () {
            msgBox.popUpOk({
                mensaje: "¿Confirma que desea eliminar el usuario?",
                titulo: "Confirmacion",
                template: "views/msjBox/msjBoxConfirmacion.html",
                controlador: "CtrlConfirmacion",
                datos: ''
            }).then(function (respuesta) {
                $scope.request.cmdj = "eliminar usuario where usuario = '" + $scope.usuario.usuario + "'";
                PeticionHttp.getAll($scope.request)
                    .then(function (data_eliminar) {
                        guardar = false;
                        Notificaciones.notificacion("CORRECTO", data_eliminar.rows[0].msj, "success", true);
                        $scope.limpiar();
                    });
            }, function () {
            });

        };
        /// TERMINA ABC USUARIO

        //ROLES
        $scope.busqueda_roles = function () {
            $scope.request = {};
            $scope.request.cmdj = "listar roles";
            PeticionHttp.getAll($scope.request)
                .then(function (data_busqueda_roles) {
                    $scope.lista_roles = data_busqueda_roles.rows;

                });
        };
        $scope.roles_usuario = function () {
            $scope.request.cmdj = "listar roles de usuario where usuario = '" + $scope.usuario.usuario + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_roles_usuario) {
                    $scope.lista_roles_usuario = data_roles_usuario.rows;
                });
        };
        $scope.asignar_rol = function () {
            $scope.request.cmdj = "asignar rol a usuario where usuario = '" + $scope.usuario.usuario + "' and rol =  '" + $scope.usuario.rol.rol + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_asignar_rol) {
                    Notificaciones.notificacion("CORRECTO", data_asignar_rol.rows[0].msj, "success", true);
                    $scope.roles_usuario();
                });
        };
        $scope.quitar_rol = function (registro_rol) {
            $scope.request.cmdj = "eliminar rol de usuario where usuario = '" + $scope.usuario.usuario + "' and rol =  '" + registro_rol.rol + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_quitar_rol) {
                    Notificaciones.notificacion("CORRECTO", data_quitar_rol.rows[0].msj, "success", true);
                    $scope.roles_usuario();
                });
        };
        //TERMINA ROLES

        //RESTABLECER CONTRASEÑA
        $scope.guar_contrasena = function () {
            if ($scope.usuario.nueva_contrasena != $scope.usuario.conf_contrasena) {
                Notificaciones.notificacion("ERROR", "No coinciden las contraseñas", "error", true);
                return;
            };
            if ($scope.usuario.nueva_contrasena == '' || $scope.usuario.nueva_contrasena == undefined || $scope.usuario.conf_contrasena == '' || $scope.usuario.conf_contrasena == undefined) {
                Notificaciones.notificacion("ERROR", "Debe ingresar una contraseña y confirmarla", "error", true);
                return;
            };
            $scope.request.cmdj = "restablecer_password where usuario = '" + $scope.usuario.usuario + "' and nuevopass =  '" + $scope.usuario.nueva_contrasena + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_guar_cont) {
                    Notificaciones.notificacion("CORRECTO", data_guar_cont.rows[0].msj, "success", true);
                    $scope.limpiar();
                });
        };
        //TERMINA RESTABLECER

        $scope.busqueda_puestos();
        $scope.busqueda_roles();

    });