<<<<<<< HEAD
angular.module('webapp')
    .controller('CtrlReporteAsistencia', function ($scope, msgBox, PeticionHttp, $location, pasoInfo, Notificaciones, mdcDateTimeDialog ) {

        $scope.date = new moment().format("YYYY/MM/D");

        console.info($scope.date);

        $scope.today = function() {
            $scope.dt = new Date();
          };
          $scope.today();
        
          $scope.clear = function() {
            $scope.dt = null;
          };
        
          $scope.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
          };
        
          $scope.dateOptions = {
            dateDisabled: disabled,
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
          };
        
          // Disable weekend selection
          function disabled(data) {
            var date = data.date,
              mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
          }
        
          $scope.toggleMin = function() {
            $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
            $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
          };
        
          $scope.toggleMin();
        
          $scope.open1 = function() {
            $scope.popup1.opened = true;
          };
        
          $scope.open2 = function() {
            $scope.popup2.opened = true;
          };
        
          $scope.setDate = function(year, month, day) {
            $scope.dt = new Date(year, month, day);
          };
        
          $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
          $scope.format = $scope.formats[0];
          $scope.altInputFormats = ['M!/d!/yyyy'];
        
          $scope.popup1 = {
            opened: false
          };
        
          $scope.popup2 = {
            opened: false
          };
        
          var tomorrow = new Date();
          tomorrow.setDate(tomorrow.getDate() + 1);
          var afterTomorrow = new Date();
          afterTomorrow.setDate(tomorrow.getDate() + 1);
          $scope.events = [
            {
              date: tomorrow,
              status: 'full'
            },
            {
              date: afterTomorrow,
              status: 'partially'
            }
          ];
        
          function getDayClass(data) {
            var date = data.date,
              mode = data.mode;
            if (mode === 'day') {
              var dayToCheck = new Date(date).setHours(0,0,0,0);
        
              for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
        
                if (dayToCheck === currentDay) {
                  return $scope.events[i].status;
                }
              }
            }
        
            return '';
          }
          
    });
||||||| merged common ancestors
=======
angular.module('webapp')
  .controller('CtrlReporteAsistencia', function ($scope, msgBox, PeticionHttp, $location, pasoInfo, Notificaciones) {
    $scope.reporte_asistencia = {};
    $scope.request = {};
    //CONFIGURACIONES
    $scope.moment = moment;
    $scope.date = new Date();
    $scope.addFifteen = function () {
      $scope.date = new Date($scope.date.getTime() + (15 * 60000));
    };

    $scope.txtfechainicial = {
      txt: {
        placeholder: 'Seleccione fecha',
        label: 'Fecha inicial',
        icono: 'fa-calendar',
        password: false
      },
      boton: {
        encendido: false,
        iconoBoton: 'fa-save',
        color: 'default',
        cargandoBoton: false,
      },
      datos: {}
    };
    $scope.txtfechafin = {
      txt: {
        placeholder: 'Seleccione fecha',
        label: 'Fecha fin',
        icono: 'fa-calendar',
        password: false
      },
      boton: {
        encendido: false,
        iconoBoton: 'fa-save',
        color: 'default',
        cargandoBoton: false,
      },
      datos: {}
    };
    $scope.boton_buscar = {
      color: $scope.color_elementos,
      tamano: 'sm',
      texto: 'Buscar',
      cargando_boton: false,
      icono: 'fa-search',
      datos: {}
    };
    $scope.tabla = {
      nocolapsado: [
        {
          alias: 'USUARIO',
          campo: 'usuario',
          tamano: '1'
        },
        {
          alias: 'NOMBRE',
          campo: 'nombre',
          tamano: '2'
        },
        {
          alias: 'NUM. EMPLEADO',
          campo: 'numempleado',
          tamano: '2'
        },
        {
          alias: 'FECHA/HORA ENTRADA',
          campo: 'fhentrada',
          tamano: '2'
        },
        {
          alias: 'FECHA/HORA SALIDA',
          campo: 'fhsalida',
          tamano: '2'
        },
        {
          alias: 'MOVIMIENTO',
          campo: 'movimiento_desc',
          tamano: '1'
        },
        {
          alias: 'DETENIDO',
          campo: 'detenido_hrs',
          tamano: '1'
        },
        {
          alias: 'TRABAJANDO',
          campo: 'trabajo_hrs',
          tamano: '1'
        }
      ],
      colapsado: ['nombre', 'fhentrada', 'fhsalida'],
      colapsarsiempre: false,
      func: function (registro) {
      }
    };
    //CONFIGURACIONES

    $scope.buscar = function () {
      $scope.boton_buscar.cargando_boton = true;
      $scope.request.cmdj = "listar asistencia where desde= '" + $scope.reporte_asistencia.fecha_inicio + "' and hasta= '" + $scope.reporte_asistencia.fecha_fin + "'";
      PeticionHttp.getAll($scope.request)
        .then(function (data_busqueda) {
          $scope.datos_busqueda = data_busqueda.rows;
          $scope.boton_buscar.cargando_boton = false;
        }, function () {
          $scope.boton_buscar.cargando_boton = false;
        });
    }


  });
>>>>>>> desarrollo
