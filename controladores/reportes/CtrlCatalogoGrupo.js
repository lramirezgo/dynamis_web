angular.module('webapp')
   .controller('CtrlCatalogoGrupo', function ($scope, msgBox, PeticionHttp, Notificaciones, $rootScope) {
      $scope.request = {};
      $scope.busqueda_grupo = {};
      $scope.boton_buscar = {
         color: 'primary',
         tamano: 'flat',
         texto: 'Buscar',
         cargando_boton: false,
         icono: 'fa-search',
         datos: {}
      };
      $scope.boton_limpiar = {
         color: 'primary',
         tamano: 'flat',
         texto: 'Limpiar',
         cargando_boton: false,
         icono: 'fa-eraser',
         datos: {}
      };
      $scope.tabla_busqueda = {
         nocolapsado: [
            {
               alias: 'CODIGO',
               campo: 'codigo',
               tamano: '2'
            },
            {
               alias: 'COD. ALTERNO',
               campo: 'mostrarcodigo',
               tamano: '2'
            },
            {
               alias: 'DESCRIPCION',
               campo: 'descripcion',
               tamano: '4'
            },
            {
               alias: 'TIPO',
               campo: 'tipo',
               tamano: '1'
            },
            {
               alias: 'PRECIO GRP',
               campo: 'preciogrp',
               tamano: '1'
            },
            {
               alias: 'GRUPO',
               campo: 'grp',
               tamano: '1'
            }
            ,
            {
               alias: 'SUB GRUPO',
               campo: 'subgrp',
               tamano: '1'
            }

         ],
         colapsado: ['codigo', 'descripcion', 'tipopreciogrp', 'precio', 'preciogrp', 'grp', 'subgrp'],
         func: function (registro) {
         }
      };
      $scope.lista_tipo_remisiones = function () {
         $scope.cargando = true;
         $scope.request.cmdj = "listar codigos where grp = 'TIPOREM'";
         PeticionHttp.getAll($scope.request)
            .then(function (data) {
               $scope.datos_lista_tipo_rem = data.rows;
               $scope.cargando = false;
            });
      };

      $scope.buscar = function () {
         $scope.boton_buscar.cargando_boton = true;
         $scope.request.cmdj = "listar catalogo grupos where tiporem = '" + $scope.busqueda_grupo.tipo + "'";
         PeticionHttp.getAll($scope.request)
            .then(function (data_busqueda) {
               $scope.datos_busqueda = data_busqueda.rows;
               $scope.boton_buscar.cargando_boton = false;
            }, function () {
               $scope.boton_buscar.cargando_boton = false;
            });
      };

      $scope.limpiar = function () {
         $scope.busqueda_grupo = {};
         $scope.datos_busqueda = '';
      };
      $scope.lista_tipo_remisiones();
   });