angular.module('webapp')
    .controller('CtrlAbcArmados', function ($scope, msgBox, PeticionHttp, Notificaciones, $uibModal, $rootScope, $mdDialog, Upload) {
        $scope.titulo = 'ABC armados';
        $scope.request = {};
        $scope.producto = {};
        $scope.componente = {};
        var guardar = true;
        var guardar_cod_alterno = true;
        var guardar_comp = true;
        Notificaciones.banderaNotificacion = true;
        $scope.producto.alterno = {};
        $scope.producto.alterno.mostrar = '0';
        $scope.producto.cveunimedida = 'PZ';

        ////CONFIGURACIONES
        $scope.txtcodigo = {
            txt: {
                placeholder: 'Codigo de armado',
                label: 'Codigo de armado',
                icono: ' fa-barcode',
                password: false,
                disabled: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-flash',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtdescripcion = {
            txt: {
                placeholder: 'Descripcion',
                label: 'Descripcion',
                icono: 'fa-file-text',
                password: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-save',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtcodigofamilia = {
            txt: {
                placeholder: '',
                label: 'Codigo familia',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: true,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtdescfamilia = {
            txt: {
                placeholder: '',
                label: 'Descripcion familia',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtgrp = {
            txt: {
                placeholder: 'Grupo',
                label: 'Grupo',
                icono: 'fa-cubes',
                password: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-save',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtsubgrp = {
            txt: {
                placeholder: 'Subgrupo',
                label: 'Subgrupo',
                icono: 'fa-cubes',
                password: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-save',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtcveunimedida = {
            txt: {
                placeholder: 'Unidad de medida',
                label: 'Unidad de medida',
                icono: 'fa-arrows-h',
                password: false,
                disabled: true
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-save',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.boton_guardar_producto = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Guardar',
            cargando_boton: false,
            icono: 'fa-save',
            datos: {}
        };
        $scope.txtcodigoalterno = {
            txt: {
                placeholder: 'Codigo alterno',
                label: 'Codigo alterno',
                icono: 'fa-barcode',
                password: false,
                disabled: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-flash',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.tabla_alterno = {
            nocolapsado: [
                {
                    alias: 'CODIGO',
                    campo: 'codigo',
                    tamano: '4'
                },
                {
                    alias: 'CODIGO ALTERNO',
                    campo: 'codigoalt',
                    tamano: '4'
                },
                {
                    alias: 'VISUALIZAR',
                    campo: 'mostrar',
                    tamano: '4'
                }
            ],
            colapsado: ['codigo', 'codigoalt', 'mostrar'],
            colapsarsiempre: false,
            func: function (registro) {
                $scope.producto.alterno = registro;
                guardar_cod_alterno = false;
            }
        };
        $scope.boton_guardar_alt = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Guardar codigo alterno',
            cargando_boton: false,
            icono: 'fa-save',
            datos: {}
        };
        $scope.boton_limpiar_alt = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Limpiar codigo alterno',
            cargando_boton: false,
            icono: 'fa-eraser',
            datos: {}
        };
        $scope.tabla_lineas = {
            nocolapsado: [
                {
                    alias: 'CODIGO',
                    campo: 'codcomponente',
                    tamano: '1'
                },
                {
                    alias: 'DESCRIPCION',
                    campo: 'descripcion',
                    tamano: '1'
                },
                {
                    alias: 'PIEZAS',
                    campo: 'piezas',
                    tamano: '1'
                },
                {
                    alias: 'ADICIONAL',
                    campo: 'adicional',
                    tamano: '1'
                },
                {
                    alias: 'LARGO',
                    campo: 'largo',
                    tamano: '1'
                },
                {
                    alias: 'ANCHO',
                    campo: 'ancho',
                    tamano: '1'
                },
                {
                    alias: 'ALTO',
                    campo: 'alto',
                    tamano: '1'
                },
                {
                    alias: 'AREA',
                    campo: 'area',
                    tamano: '1'
                },
                {
                    alias: 'LINEAL',
                    campo: 'lineal',
                    tamano: '1'
                },
                {
                    alias: 'GRAMOS',
                    campo: 'gramos',
                    tamano: '1'
                },
                {
                    alias: 'UNIDADES X PZA',
                    campo: 'unixpza',
                    tamano: '1'
                },
                {
                    alias: 'UNIDAD DE MEDIDA',
                    campo: 'cveunimedida',
                    tamano: '1'
                }
            ],
            colapsado: ['codigo', 'codigoalt', 'mostrar'],
            colapsarsiempre: false,
            func: function (registro_linea) {
                $scope.componente = registro_linea;
                $scope.componente.codigo = registro_linea.codcomponente;
                $scope.componente.unidad = registro_linea.cveunimedida;
                guardar_comp = false;
                $scope.orden = false;
            }
        };
        $scope.boton_abrir_componentes = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: '',
            cargando_boton: false,
            icono: 'fa-th-list',
            datos: {}
        };

        ///END CONFIGURACIONES

        $scope.abrir_catalogo = function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de armados",
                template: "views/msjBox/msjBoxCatArmados.html",
                controlador: "CtrlCatArmados",
                datos: ""
            }).then(function (respuesta) {
                $scope.producto = respuesta;
                guardar = false;
                $scope.buscar_cod_alterno();
                $scope.buscar_lineas_armado();

            }, function () {

            });
        };
        $scope.abrir_catalogo_familia = function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de familias",
                template: "views/msjBox/msjBoxCatFamilias.html",
                controlador: "CtrlCatFamilias",
                datos: 'ARMADO'
            })
                .then(function (respuesta) {
                    $scope.producto.cvefamilia = respuesta.codigo;
                    $scope.producto.familia = respuesta.descripcion;
                }, function () {
                });
        };
        $scope.limpiar = function () {
            $scope.request = {};
            $scope.producto = {};
            guardar = true;
            $scope.producto.alterno = {};
            $scope.producto.alterno.mostrar = '0';
            $scope.datos_lista_lin_armado = '';
            $scope.datos_lista_alterno = '';
        };

        // ABC ARMADO
        $scope.busqueda_familias = function () {
            $scope.request = {};
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar catalogo where grp = 'CVEFAMILIA' and subgrp = 'ARMADO'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_busqueda_familias) {
                    $scope.lista_familias = data_busqueda_familias.rows;
                    // console.info(data.data.rows[0].msj);

                });
        };
        $scope.guardar = function () {
            $scope.boton_guardar_producto.cargando_boton = true;
            if (guardar) {
                $scope.guardar_armado();
            } else {
                $scope.editar_armado();
            }

        };
        $scope.guardar_armado = function () {
            
            if ($scope.producto.codigo == "" || $scope.producto.codigo == undefined || $scope.producto.descripcion == "" || $scope.producto.descripcion == undefined || $scope.producto.cvefamilia == "" || $scope.producto.cvefamilia == undefined || $scope.producto.cveunimedida == "" || $scope.producto.cveunimedida == undefined) {
                Notificaciones.notificacion("ERROR", "Debe llenar todos los datos", "error", true);
                $scope.boton_guardar_producto.cargando_boton = false;
                return;
            };
            $scope.request.tipo = "request";
            $scope.request.cmdj = "crear_armado where armado = '" + $scope.producto.codigo + "' and cvefamilia = '" + $scope.producto.cvefamilia + "' and cveunimedida = '" + $scope.producto.cveunimedida + "' and  descripcion = '" + $scope.producto.descripcion + "'  and  grp = '" + $scope.producto.grp + "' and  subgrp = '" + $scope.producto.subgrp + "' and desc_corta = '' ";
            PeticionHttp.getAll($scope.request)
                .then(function (data_guardar_cabecera) {
                    guardar = false;
                    Notificaciones.notificacion("CORRECTO", data_guardar_cabecera.rows[0].msj, "success", true);
                    $scope.boton_guardar_producto.cargando_boton = false;

                }, function () {
                    $scope.boton_guardar_producto.cargando_boton = false;
                });

        };
        $scope.editar_armado = function () {
            if ($scope.producto.codigo == "" || $scope.producto.codigo == undefined || $scope.producto.descripcion == "" || $scope.producto.descripcion == undefined || $scope.producto.cvefamilia == "" || $scope.producto.cvefamilia == undefined || $scope.producto.cveunimedida == "" || $scope.producto.cveunimedida == undefined) {
                Notificaciones.notificacion("ERROR", "Debe llenar todos los datos", "error", true);
                $scope.boton_guardar_producto.cargando_boton = false;
                return;
            };
            $scope.request.tipo = "request";
            $scope.request.cmdj = "actualizar_armado where armado = '" + $scope.producto.codigo + "' and cvefamilia = '" + $scope.producto.cvefamilia + "' and grp = '" + $scope.producto.grp + "' and subgrp = '" + $scope.producto.subgrp + "'  and cveunimedida = '" + $scope.producto.cveunimedida + "' and  descripcion = '" + $scope.producto.descripcion + "' and desc_corta = '' ";
            PeticionHttp.getAll($scope.request)
                .then(function (data_editar_cabecera) {
                    guardar_componente = false;
                    Notificaciones.notificacion("CORRECTO", data_editar_cabecera.rows[0].msj, "success", true);
                    $scope.boton_guardar_producto.cargando_boton = false;
                }, function () {
                    $scope.boton_guardar_producto.cargando_boton = false;
                });
        };
        $scope.eliminar_armado = function () {
            msgBox.popUpOk({
                mensaje: "¿Confirma que desea eliminar el armado?",
                titulo: "Confirmacion",
                template: "views/msjBox/msjBoxConfirmacion.html",
                controlador: "CtrlConfirmacion",
                datos: ''
            }).then(function (respuesta) {
                $scope.request.tipo = "request";
                $scope.request.cmdj = "eliminar armado where codigo = '" + $scope.producto.codigo + "'";
                PeticionHttp.getAll($scope.request)
                    .then(function (data_eliminar_cabecera) {
                        guardar_componente = true;
                        Notificaciones.notificacion("CORRECTO", data_eliminar_cabecera.rows[0].msj, "success", true);
                        $scope.limpiar();

                    });
            }, function () {
            });
        };
        // TEMRINA ABC ARMADO
        ////CODIGO ALTERNO
        $scope.busqueda_tipos_alt = function () {
            $scope.request = {};
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar codigos where grp = 'TIPOCODIGOALT'  and estado = 'A' ";
            PeticionHttp.getAll($scope.request)
                .then(function (data_tipos_alt) {
                    $scope.lista_tipos_alt = data_tipos_alt.rows;
                });

        };
        $scope.guardar_alt = function () {
            if (guardar_cod_alterno) {
                $scope.confirmar_guardar_alt();
            } else {
                $scope.confirmar_edicion_alt();
            }

        };
        $scope.confirmar_guardar_alt = function () {
            if ($scope.producto.alterno.codigoalt == "" || $scope.producto.alterno.codigoalt == undefined || $scope.producto.alterno.tipocodigoalt == "" || $scope.producto.alterno.tipocodigoalt == undefined || $scope.producto.alterno.mostrar == "" || $scope.producto.alterno.mostrar == undefined) {
                Notificaciones.notificacion("ERROR", "Debe ingresar un codigo alterno", "error", true);
                return;
            };
            $scope.request.tipo = "request";
            $scope.request.cmdj = "crear codigo alterno where codigo = '" + $scope.producto.codigo + "' and codigoalt = '" + $scope.producto.alterno.codigoalt + "' and tipo = '" + $scope.producto.alterno.tipocodigoalt + "' and mostrar = '" + $scope.producto.alterno.mostrar + "'  ";
            console.info($scope.request.cmdj);
            PeticionHttp.getAll($scope.request)
                .then(function (data_guardar_alterno) {
                    Notificaciones.notificacion("CORRECTO", data_guardar_alterno.rows[0].msj, "success", true);
                    $scope.buscar_cod_alterno();
                });
        };
        $scope.confirmar_edicion_alt = function () {
            $scope.request.tipo = "request";
            $scope.request.cmdj = "actualizar codigo alterno where  codigo = '" + $scope.producto.alterno.codigo + "' and codigoalt  = '" + $scope.producto.alterno.codigoalt + "' and  tipo = '" + $scope.producto.alterno.tipocodigoalt + "' and mostrar = '" + $scope.producto.alterno.mostrar + "' ";
            console.info($scope.request.cmdj);
            PeticionHttp.getAll($scope.request)
                .then(function (data_guardar_cambio_alt) {
                    Notificaciones.notificacion("CORRECTO", data_guardar_cambio_alt.rows[0].msj, "success", true);
                    $scope.buscar_cod_alterno();
                });
        };
        $scope.buscar_cod_alterno = function () {
            $scope.request = {};
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar codigo alterno where codigo = '" + $scope.producto.codigo + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_buscar_codalt) {
                    $scope.datos_lista_alterno = data_buscar_codalt.rows;
                    $scope.num_registros = $scope.datos_lista_alterno.length;
                    $scope.num_paginas = (Math.ceil($scope.datos_lista_alterno.length / 10));
                    $scope.cargando = false;
                }, function () {
                    $scope.datos_lista_alterno = '';
                });

        };
        $scope.seleccionar_alterno = function (codigo_alterno) {
            $scope.producto.alterno = codigo_alterno;
            guardar_cod_alterno = false;
        };
        $scope.limpiar_alt = function () {
            $scope.producto.alterno = {};
            guardar_cod_alterno = true;
        };
        ///TERMINA CODIGO ALTERNO

        // LISTA DE ARMADO
        $scope.abrir_catalogo_componentes = function () {
            if ($scope.producto.codigo == "" || $scope.producto.codigo == undefined) {
                Notificaciones.notificacion("ERROR", "Debe selecionar un producto armado", "error", true);
                return;
            };
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de componentes",
                template: "views/msjBox/msjBoxCatComponentes.html",
                controlador: "CtrlCatComponentes",
                datos: ""
            })
                .then(function (respuesta) {
                    $scope.componente = respuesta;
                }, function () {
                });
        };
        $scope.buscar_lineas_armado = function () {
            $scope.request = {};
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar lineas armado where codarmado = '" + $scope.producto.codigo + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_lineas_armado) {
                    $scope.datos_lista_lin_armado = data_lineas_armado.rows;
                    $scope.cargando = false;
                }, function () {
                    $scope.datos_lista_lin_armado = '';
                });
        };
        $scope.guardar_componente = function () {
            if ($scope.producto.codigo == "" || $scope.producto.codigo == undefined) {
                Notificaciones.notificacion("CORRECTO", "Debe selecionar un producto armado", "error", true);
                return;
            };
            if (guardar_comp) {
                $scope.guardar_linea_componente();
            } else {
                $scope.editar_linea_componente();
            }

        };
        $scope.guardar_linea_componente = function () {
            if ($scope.componente.piezas == "" || $scope.componente.piezas == undefined || $scope.componente.piezas == '0') {
                Notificaciones.notificacion("CORRECTO", "Debe llenar el dato de pieza o bien el dato debe ser mayor a 0", "error", true);
                return;
            };
            if ($scope.componente.unidad == 'CM2') {
                if ($scope.componente.largo == "" || $scope.componente.largo == undefined || $scope.componente.ancho == "" || $scope.componente.ancho == undefined || $scope.componente.largo == '0' || $scope.componente.ancho == '0') {
                    Notificaciones.notificacion("CORRECTO", "Debe llenar todos los datos de largo y ancho o bien el dato debe ser mayor a 0", "error", true);
                    return;
                }
            };

            if ($scope.componente.unidad == 'GRAMOS') {
                if ($scope.componente.gramos == "" || $scope.componente.gramos == undefined || $scope.componente.gramos == '0') {
                    if ($scope.componente.largo == "" || $scope.componente.largo == undefined || $scope.componente.ancho == "" || $scope.componente.ancho == undefined || $scope.componente.largo == '0' || $scope.componente.ancho == '0') {
                        Notificaciones.notificacion("CORRECTO", "Debe ingresar los gramos o bien el dato debe ser mayor a 0", "error", true);
                        return;
                    };
                }
            };

            if ($scope.componente.unidad == 'CM LINEALES') {
                if ($scope.componente.largo == "" || $scope.componente.largo == undefined || $scope.componente.largo == '0') {
                    Notificaciones.notificacion("CORRECTO", "Debe llenar el dato de largo o bien el dato debe ser mayor a 0", "error", true);
                    return;
                };
            };
            $scope.componente.adicional = $scope.componente.adicional / 100;

            $scope.request.tipo = "request";
            $scope.request.cmdj = "crear linea armado where codarmado= '" + $scope.producto.codigo + "'  and codcomponente = '" + $scope.componente.codigo + "'  and piezas = '" + $scope.componente.piezas + "' and adicional = '" + $scope.componente.adicional + "' and largo = '" + $scope.componente.largo + "' and ancho ='" + $scope.componente.ancho + "' and gramos = '" + $scope.componente.gramos + "';";

            PeticionHttp.getAll($scope.request)
                .then(function (data_guardar_linea) {
                    guardar_comp = true;
                    Notificaciones.notificacion("CORRECTO", data_guardar_linea.rows[0].msj, "success", true);
                    $scope.buscar_lineas_armado();
                    $scope.limpiar_componente();
                });

        };
        $scope.editar_linea_componente = function () {
            if ($scope.componente.piezas == "" || $scope.componente.piezas == undefined || $scope.componente.piezas == '0') {
                Notificaciones.notificacion("CORRECTO", "Debe llenar el dato de pieza o bien el dato debe ser mayor a 0", "error", true);
                return;
            };
            if ($scope.componente.unidad == 'CM2') {
                if ($scope.componente.largo == "" || $scope.componente.largo == undefined || $scope.componente.ancho == "" || $scope.componente.ancho == undefined || $scope.componente.largo == '0' || $scope.componente.ancho == '0') {
                    Notificaciones.notificacion("CORRECTO", "Debe llenar todos los datos de largo y ancho o bien el dato debe ser mayor a 0", "error", true);
                    return;
                }
            };

            if ($scope.componente.unidad == 'GRAMOS') {
                if ($scope.componente.gramos == "" || $scope.componente.gramos == undefined || $scope.componente.gramos == '0') {
                    if ($scope.componente.largo == "" || $scope.componente.largo == undefined || $scope.componente.ancho == "" || $scope.componente.ancho == undefined || $scope.componente.largo == '0' || $scope.componente.ancho == '0') {
                        Notificaciones.notificacion("CORRECTO", "Debe ingresar los gramos o bien el dato debe ser mayor a 0", "error", true);
                        return;
                    };
                }
            };

            if ($scope.componente.unidad == 'CM LINEALES') {
                if ($scope.componente.largo == "" || $scope.componente.largo == undefined || $scope.componente.largo == '0') {
                    Notificaciones.notificacion("CORRECTO", "Debe llenar el dato de largo o bien el dato debe ser mayor a 0", "error", true);
                    return;
                };
            };
            $scope.componente.adicional = $scope.componente.adicional / 100;
            $scope.request.tipo = "request";
            $scope.request.cmdj = "actualizar linea armado where codarmado= '" + $scope.producto.codigo + "'  and codcomponente = '" + $scope.componente.codigo + "'  and piezas = '" + $scope.componente.piezas + "' and adicional = '" + $scope.componente.adicional + "' and largo = '" + $scope.componente.largo + "' and ancho ='" + $scope.componente.ancho + "' and gramos = '" + $scope.componente.gramos + "' and orden = '" + $scope.componente.orden + "';";
            PeticionHttp.getAll($scope.request)
                .then(function (data_editar_linea) {
                    guardar_comp = true;
                    Notificaciones.notificacion("CORRECTO", data_editar_linea.rows[0].msj, "success", true);
                    $scope.buscar_lineas_armado();
                    $scope.limpiar_componente();
                });
        };
        $scope.limpiar_componente = function () {
            $scope.componente = {};
            var guardar_comp = true;
            $scope.buscar_lineas_armado();
        };
        $scope.seleccionar_linea = function (registro_linea) {
            $scope.componente = registro_linea;
            $scope.componente.codigo = registro_linea.codcomponente;
            $scope.componente.unidad = registro_linea.cveunimedida;
            guardar_comp = false;
            $scope.orden = false;
        };
        $scope.eliminar_linea = function () {
            msgBox.popUpOk({
                mensaje: "¿Confirma que desea eliminar el armado?",
                titulo: "Confirmacion",
                template: "views/msjBox/msjBoxConfirmacion.html",
                controlador: "CtrlConfirmacion",
                datos: ''
            }).then(function (respuesta) {
            $scope.request.tipo = "request";
            $scope.request.cmdj = "eliminar linea armado where armado = '" + $scope.producto.codigo + "'  and orden = '" + $scope.componente.orden + "' and componente = '" + $scope.componente.codigo + "'";

            PeticionHttp.getAll($scope.request)
                .then(function (data_eliminar_linea) {
                    guardar_comp = true;
                    Notificaciones.notificacion("CORRECTO", data_eliminar_linea.rows[0].msj, "success", true);
                    $scope.buscar_lineas_armado();
                    $scope.limpiar_componente();
                });
            }, function () {
            });

        };
        // TEMRINA LISTA DE ARMADO
        $scope.busqueda_familias();
        $scope.busqueda_tipos_alt();

    });