angular.module('webapp')
    .controller('CtrlAbcCatalogos', function ($scope, msgBox, PeticionHttp, Notificaciones, $uibModal, $rootScope, $mdDialog, Upload) {

        $scope.request = {};
        $scope.registro_catalogo = {};
        Notificaciones.banderaNotificacion = true;
        $scope.titulo = 'ABC Catalogos';
        var guardar = true;
        $scope.mostrar_codigo = false;

        //  CONFIGURACIONES  

        $scope.tabla = {
            nocolapsado: [
                {
                    alias: 'CODIGO',
                    campo: 'codigo',
                    tamano: '2'
                },
                {
                    alias: 'DESCRIPCION',
                    campo: 'descripcion',
                    tamano: '4'
                },
                {
                    alias: 'ORDEN',
                    campo: 'orden',
                    tamano: '2'
                },
                {
                    alias: 'GRUPO',
                    campo: 'grp',
                    tamano: '2'
                },
                {
                    alias: 'SUB GRUPO',
                    campo: 'subgrp',
                    tamano: '2'
                }
            ],
            colapsado: ['codigo', 'descripcion'],
            colapsarsiempre: false,
            func: function (registro) {
                $scope.registro_catalogo = registro;
                guardar = false;
                $scope.mostrar_codigo = true;
            }
        };
        $scope.txtcodigo = {
            txt: {
                placeholder: 'Ingrese Codigo',
                label: 'Codigo',
                icono: 'fa-barcode',
                password: false,
                disabled: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-flash',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtdsc = {
            txt: {
                placeholder: 'Ingrese descripcion',
                label: 'Descripcion',
                icono: 'fa-file-text-o',
                password: false,
                disabled: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-flash',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtdsccorta = {
            txt: {
                placeholder: 'Ingrese descripcion corta',
                label: 'Descripcion corta',
                icono: 'fa-file-text',
                password: false,
                disabled: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-flash',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtgrp = {
            txt: {
                placeholder: 'Ingrese grupo',
                label: 'Grupo',
                icono: 'fa-object-ungroup',
                password: false,
                disabled: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-flash',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtsubgrp = {
            txt: {
                placeholder: 'Ingrese subgrupo',
                label: 'Subgrupo',
                icono: 'fa-object-group',
                password: false,
                disabled: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-flash',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtorden = {
            txt: {
                placeholder: 'Ingrese orden',
                label: 'Orden',
                icono: 'fa-list-ol',
                password: false,
                disabled: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-flash',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.boton_guardar = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Guardar',
            cargando_boton: false,
            icono: 'fa-save',
            datos: {}
        };
        $scope.boton_limpiar = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Limpiar',
            cargando_boton: false,
            icono: 'fa-eraser',
            datos: {}
        };
        $scope.boton_eliminar = {
            color: 'danger',
            tamano: 'sm',
            texto: 'Eliminar',
            cargando_boton: false,
            icono: 'fa-trash',
            datos: {}
        };
        // END CONFIGURACIONES
        $scope.lista_catalogo = function () {
            /* if(catalogos.datos_lista_productos != ""){
               console.info("ya tengo los datos, solo los muestro");  
               $scope.datos_lista_productos = catalogos.datos_lista_productos;
               $scope.hablitarcargando=false;
               $scope.hablitartabla=true;
               return;
             }*/
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar codigos where estado = 'A'";

            PeticionHttp.getAll($scope.request)
                .then(function (data_catalogo) {
                    $scope.datos_lista = data_catalogo.rows;
                    $scope.cargando = false;
                });
        };
        $scope.limpiar = function () {
            $scope.registro_catalogo = {};
            $scope.mostrar_codigo = false;
            var guardar = true;
        };
        // SELECCION
        $scope.seleccionar_grupo = function (grupo) {
            $scope.registro_catalogo.grp = grupo;
        };
        $scope.seleccionar_subgrupo = function (subgrupo) {
            $scope.registro_catalogo.subgrp = subgrupo;
        };
        $scope.seleccionar_registro = function (elemento) {
            $scope.registro_catalogo = elemento;
            guardar = false;
            $scope.mostrar_codigo = true;
        };
        // TERMINA SELECCION

        ///ABC 

        $scope.guardar = function () {
            $scope.boton_guardar.cargando_boton = true;
            if (guardar) {
                $scope.confirmar_guardar_registro();
            } else {
                $scope.confirmar_edicion_registro();
            }
        };
        $scope.confirmar_guardar_registro = function () {
            if ($scope.registro_catalogo.grp == "" || $scope.registro_catalogo.grp == undefined) {
                Notificaciones.notificacion("ERROR", "Debe ingresar un grupo", "error", true);
                $scope.boton_guardar.cargando_boton = false;
                return;
            };

            if ($scope.registro_catalogo.subgrp == "" || $scope.registro_catalogo.subgrp == undefined) {
                Notificaciones.notificacion("ERROR", "Debe ingresar un subgrupo", "error", true);
                $scope.boton_guardar.cargando_boton = false;
                return;
            };

            if ($scope.registro_catalogo.orden == "" || $scope.registro_catalogo.orden == undefined) {
                Notificaciones.notificacion("ERROR", "Debe ingresaun numero de orden", "error", true);
                $scope.boton_guardar.cargando_boton = false;
                return;
            };

            if ($scope.registro_catalogo.codigo == "" || $scope.registro_catalogo.codigo == undefined) {
                Notificaciones.notificacion("ERROR", "Debe ingresaun un codigo", "error", true);
                $scope.boton_guardar.cargando_boton = false;
                return;
            };

            if ($scope.registro_catalogo.descripcion == "" || $scope.registro_catalogo.descripcion == undefined) {
                Notificaciones.notificacion("ERROR", "Debe ingresar una descripcion", "error", true);
                $scope.boton_guardar.cargando_boton = false;
                return;
            };
            $scope.request.tipo = "request";
            $scope.request.cmdj = "crear codigo where grp = '" + $scope.registro_catalogo.grp + "' and subgrp = '" + $scope.registro_catalogo.subgrp + "' and codigo = '" + $scope.registro_catalogo.codigo + "' and descripcion = '" + $scope.registro_catalogo.descripcion + "' and desc_corta = '" + $scope.registro_catalogo.desc_corta + "' and estado = 'A' and orden = '" + $scope.registro_catalogo.orden + "'";

            PeticionHttp.getAll($scope.request)
                .then(function (data_guardar) {
                    guardar = true;
                    Notificaciones.notificacion("CORRECTO", data_guardar.rows[0].msj, "success", true);
                    $scope.limpiar();
                    $scope.lista_catalogo();
                    $scope.boton_guardar.cargando_boton = false;

                },function(){
                    $scope.boton_guardar.cargando_boton = false;
                });

        };
        $scope.confirmar_edicion_registro = function () {
            if ($scope.registro_catalogo.grp == "" || $scope.registro_catalogo.grp == undefined) {
                Notificaciones.notificacion("ERROR", "Debe ingresar un grupo", "error", true);
                $scope.boton_guardar.cargando_boton = false;
                return;
            };

            if ($scope.registro_catalogo.subgrp == "" || $scope.registro_catalogo.subgrp == undefined) {
                Notificaciones.notificacion("ERROR", "Debe ingresar un subgrupo", "error", true);
                $scope.boton_guardar.cargando_boton = false;
                return;
            };

            if ($scope.registro_catalogo.orden == "" || $scope.registro_catalogo.orden == undefined) {
                Notificaciones.notificacion("ERROR", "Debe ingresaun numero de orden", "error", true);
                $scope.boton_guardar.cargando_boton = false;
                return;
            };

            if ($scope.registro_catalogo.codigo == "" || $scope.registro_catalogo.codigo == undefined) {
                Notificaciones.notificacion("ERROR", "Debe ingresaun un codigo", "error", true);
                $scope.boton_guardar.cargando_boton = false;
                return;
            };

            if ($scope.registro_catalogo.descripcion == "" || $scope.registro_catalogo.descripcion == undefined) {
                Notificaciones.notificacion("ERROR", "Debe ingresar una descripcion", "error", true);
                $scope.boton_guardar.cargando_boton = false;
                return;
            };
            $scope.request.tipo = "request";
            $scope.request.cmdj = "actualizar codigo  where grp = '" + $scope.registro_catalogo.grp + "' and subgrp = '" + $scope.registro_catalogo.subgrp + "' and codigo = '" + $scope.registro_catalogo.codigo + "' and descripcion = '" + $scope.registro_catalogo.descripcion + "' and desc_corta = '" + $scope.registro_catalogo.desc_corta + "' and orden = '" + $scope.registro_catalogo.orden + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_edicion) {
                    guardar = true;
                    Notificaciones.notificacion("CORRECTO", data_edicion.rows[0].msj, "success", true);
                    $scope.limpiar();
                    $scope.lista_catalogo();
                    $scope.boton_guardar.cargando_boton = false;

                },function(){
                    $scope.boton_guardar.cargando_boton = false;
                });

        };
        $scope.eliminar = function () {
            if ($scope.registro_catalogo.codigo == "" || $scope.registro_catalogo.codigo == undefined) {
                Notificaciones.notificacion("ERROR", "No ah selecionado algun codigo", "error", true);
                $scope.boton_eliminar.cargando_boton = false;
                return;
            };
            msgBox.popUpOk({
                mensaje: "¿Confirma que desea eliminar el codigo?",
                titulo: "Confirmacion",
                template: "views/msjBox/msjBoxConfirmacion.html",
                controlador: "CtrlConfirmacion",
                datos: ''
            }).then(function (respuesta) {
                $scope.boton_eliminar.cargando_boton = true;
                $scope.request.tipo = "request";
                $scope.request.cmdj = "eliminar codigo where grp = '" + $scope.registro_catalogo.grp + "' and subgrp = '" + $scope.registro_catalogo.subgrp + "' and codigo = '" + $scope.registro_catalogo.codigo + "'";
                PeticionHttp.getAll($scope.request)
                    .then(function (data_eliminar) {
                        guardar = true;
                        Notificaciones.notificacion("CORRECTO", data_eliminar.rows[0].msj, "success", true);
                        $scope.limpiar();
                        $scope.lista_catalogo();
                        $scope.boton_eliminar.cargando_boton = false;
                    }, function(){
                        $scope.boton_eliminar.cargando_boton = false;
                    });
            }, function () {
            });
        };
        ///TERMINA ABC
        $scope.lista_catalogo();
    });