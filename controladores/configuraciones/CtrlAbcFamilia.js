angular.module('webapp')
    .controller('CtrlAbcFamilias', function ($scope, msgBox, PeticionHttp, Notificaciones, $uibModal, $rootScope, $mdDialog, Upload) {
        $scope.titulo = 'ABC Familias';
        $scope.familia = {};
        $scope.request = {};
        var guardar = true;
        $scope.deshabilitar_boton = false;
        Notificaciones.banderaNotificacion = true;

        ///CONFIGURACIONES
        $scope.txtcodigo = {
            txt: {
                placeholder: 'Codigo familia',
                label: 'Codigo familia',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: true,
                iconoBoton: 'fa-flash',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtdescripcion = {
            txt: {
                placeholder: 'Descripcion',
                label: 'Descripcion',
                icono: 'fa-file-text',
                password: false,
                disabled: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-flash',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        ///CONFIGURACIONES
        // CATALOGO
        $scope.abrir_catalogo = function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de familias",
                template: "views/msjBox/msjBoxCatFamilias.html",
                controlador: "CtrlCatFamilias",
                datos: ''
            })
                .then(function (respuesta) {
                    $scope.familia = respuesta;
                    guardar = false;
                    $scope.deshabilitar_boton = true;

                }, function () {
                });
        };
        // TERMINA CATALOGO

        $scope.generar_folio = function () {
            $scope.txtcodigo.boton.cargandoBoton = true;
            if ($scope.familia.subgrp == "" || $scope.familia.subgrp == undefined) {
                Notificaciones.notificacion("ERROR", "Debe seleccionar primero el tipo de familia", "error", true);
                $scope.txtcodigo.boton.cargandoBoton = false;
                return;
            };
            if ($scope.familia.subgrp == 'COMPONENTE') {
                $scope.familia.subgrp_tipo = 'FAMCOM';
            };

            if ($scope.familia.subgrp == 'ARMADO') {
                $scope.familia.subgrp_tipo = 'FAMARM';
            };

            $scope.request.tipo = "request";
            $scope.request.cmdj = "crear secuencia where tipo = '" + $scope.familia.subgrp_tipo + "'";

            PeticionHttp.getAll($scope.request)
                .then(function (data_codigo) {
                    $scope.familia.codigo = data_codigo.rows[0].numsecuencia;
                    $scope.txtcodigo.boton.cargandoBoton = false;
                }, function () {
                    $scope.txtcodigo.boton.cargandoBoton = false;
                });
        };
        $scope.limpiar = function () {
            $scope.familia = {};
            guardar = true;
            $scope.deshabilitar_boton = false;
        };

        // ABC
        $scope.guardar = function () {
            if (guardar) {
                $scope.guardar_familia();
            } else {
                $scope.editar_familia();
            };

        };
        $scope.guardar_familia = function () {
            $scope.cargando = true;
            if ($scope.familia.codigo == "" || $scope.familia.codigo == undefined || $scope.familia.descripcion == "" || $scope.familia.descripcion == undefined || $scope.familia.subgrp == "" || $scope.familia.subgrp == undefined) {
                Notificaciones.notificacion("ERROR", "Debe llenar todos los datos", "error", true);
                $scope.cargando = false;
                return;
            };

            $scope.request.tipo = "request";
            $scope.request.cmdj = "crear codigo where grp = 'CVEFAMILIA' and subgrp = '" + $scope.familia.subgrp + "' and codigo = '" + $scope.familia.codigo + "' and descripcion = '" + $scope.familia.descripcion + "' and estado = 'A'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_guardar_familia) {
                    Notificaciones.notificacion("CORRECTO", data_guardar_familia.rows[0].msj, "success", true);
                    guardar = true;
                    $scope.cargando = false;
                    $scope.limpiar();
                },function(){
                    $scope.cargando = false;
                });
        };
        $scope.editar_familia = function () {
            $scope.cargando = true;
            if ($scope.familia.codigo == "" || $scope.familia.codigo == undefined || $scope.familia.descripcion == "" || $scope.familia.descripcion == undefined || $scope.familia.subgrp == "" || $scope.familia.subgrp == undefined) {
                Notificaciones.notificacion("ERROR", "Debe llenar todos los datos", "error", true);
                $scope.cargando = false;
                return;
            };

            $scope.request.tipo = "request";
            $scope.request.cmdj = "actualizar codigo where grp = 'CVEFAMILIA' and subgrp = '" + $scope.familia.subgrp + "' and codigo = '" + $scope.familia.codigo + "' and descripcion = '" + $scope.familia.descripcion + "' and estado = 'A'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_editar_familia) {
                    Notificaciones.notificacion("CORRECTO", data_editar_familia.rows[0].msj, "success", true);
                    guardar = true;
                    $scope.cargando = false;
                    $scope.limpiar();
                },function(){
                    $scope.cargando = false;
                });
        };
        $scope.eliminar_familia = function () {
            msgBox.popUpOk({
                mensaje: "¿Confirma que desea eliminar la familia?",
                titulo: "Confirmacion",
                template: "views/msjBox/msjBoxConfirmacion.html",
                controlador: "CtrlConfirmacion",
                datos: ''
            }).then(function (respuesta) {
                $scope.cargando = true;
                $scope.request.tipo = "request";
                $scope.request.rastreo = false;
                $scope.request.cmdj = "eliminar codigo where grp = 'CVEFAMILIA' and subgrp = '" + $scope.familia.subgrp + "'  and codigo = '" + $scope.familia.codigo + "' ";
                PeticionHttp.getAll($scope.request)
                    .then(function (data_eliminar_familia) {
                        Notificaciones.notificacion("CORRECTO", data_eliminar_familia.rows[0].msj, "success", true);
                        guardar = true;
                        $scope.cargando = false;
                        $scope.limpiar();
                    },function(){
                        $scope.cargando = false;
                    });
            }, function () {
            });
        };
        //TERMINA ABC

    });