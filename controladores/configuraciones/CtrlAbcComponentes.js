angular.module('webapp')
    .controller('CtrlAbcComponentes', function ($scope, msgBox, PeticionHttp, Notificaciones, $uibModal, $rootScope, $mdDialog, Upload) {
        $scope.titulo = 'ABC componentes';
        $scope.request = {};
        $scope.producto = {};
        var guardar_componente = true;
        var guardar_cod_alterno = true;
        var guardar_precios = true;
        Notificaciones.banderaNotificacion = true;
        var pc = this;
        $scope.producto.alterno = {};
        $scope.producto.alterno.mostrar = '0';

        ///CONFIGURACIONES
        $scope.txtcodigo = {
            txt: {
                placeholder: 'Codigo componente',
                label: 'Codigo componente',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: true,
                iconoBoton: 'fa-flash',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtdescripcion = {
            txt: {
                placeholder: 'Descripcion',
                label: 'Descripcion',
                icono: 'fa-file-text',
                password: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-save',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtcodigofamilia = {
            txt: {
                placeholder: '',
                label: 'Codigo familia',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: true,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtdescfamilia = {
            txt: {
                placeholder: '',
                label: 'Descripcion familia',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtcodigounidad = {
            txt: {
                placeholder: '',
                label: 'Codigo unidad',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: true,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtdescunidad = {
            txt: {
                placeholder: '',
                label: 'Descripcion familia',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.boton_guardar_producto = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Guardar',
            cargando_boton: false,
            icono: 'fa-save',
            datos: {}
        };
        $scope.txtcodigoalterno = {
            txt: {
                placeholder: 'Codigo alterno',
                label: 'Codigo alterno',
                icono: 'fa-arrows-h',
                password: false,
                disabled: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-flash',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.tabla_alterno = {
            nocolapsado: [
                {
                    alias: 'CODIGO',
                    campo: 'codigo',
                    tamano: '4'
                },
                {
                    alias: 'CODIGO ALTERNO',
                    campo: 'codigoalt',
                    tamano: '4'
                },
                {
                    alias: 'VISUALIZAR',
                    campo: 'mostrar',
                    tamano: '4'
                }
            ],
            colapsado: ['codigo', 'codigoalt', 'mostrar'],
            colapsarsiempre: false,
            func: function(registro){
                $scope.producto.alterno = registro;
                guardar_cod_alterno = false;
            }
        };
        $scope.boton_guardar_alt = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Guardar',
            cargando_boton: false,
            icono: 'fa-save',
            datos: {}
        };
        $scope.boton_limpiar_alt = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Limpiar',
            cargando_boton: false,
            icono: 'fa-eraser',
            datos: {}
        };
        $scope.txtprecio= {
            txt: {
                placeholder: 'Precio',
                label: 'Precio',
                icono: 'fa-money',
                password: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-save',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.u_pza= {
            txt: {
                placeholder: 'Unidades por pieza',
                label: 'Unidades por pieza',
                icono: 'fa-cubes',
                password: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-save',
                color: 'default',
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.boton_guardar_precio = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Guardar',
            cargando_boton: false,
            icono: 'fa-save',
            datos: {}
        };
        $scope.boton_eliminar_precio = {
            color: 'danger',
            tamano: 'sm',
            texto: 'Eliminar',
            cargando_boton: false,
            icono: 'fa-trash',
            datos: {}
        };
        $scope.boton_limpiar_precio = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Limpiar',
            cargando_boton: false,
            icono: 'fa-eraser',
            datos: {}
        };
        $scope.tabla_precios = {
            nocolapsado: [
                {
                    alias: 'CODIGO',
                    campo: 'codigo',
                    tamano: '3'
                },
                {
                    alias: 'TIPO PRECIO',
                    campo: 'tipoprecio',
                    tamano: '3'
                },
                {
                    alias: 'PRECIO',
                    campo: 'precio',
                    tamano: '3'
                },
                {
                    alias: 'UNIDADES POR PIEZA',
                    campo: 'unixpza',
                    tamano: '3'
                }
            ],
            colapsado: ['codigo', 'tipoprecio', 'precio', 'unixpza'],
            colapsarsiempre: false,
            func: function(registro){
                $scope.producto.precios = registro;
            guardar_precios = false;
            }
        };
        $scope.boton_abrir_catalogo = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: '',
            cargando_boton: false,
            icono: 'fa-th-list',
            datos: {}
        };
        $scope.boton_eliminar_producto = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: '',
            cargando_boton: false,
            icono: 'fa-trash',
            datos: {}
        };
        $scope.boton_limpiar_pantalla = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: '',
            cargando_boton: false,
            icono: 'fa-eraser',
            datos: {}
        };
        ///END CONFIGURACIONES

        $scope.abrir_catalogo = function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de componentes",
                template: "views/msjBox/msjBoxCatComponentes.html",
                controlador: "CtrlCatComponentes",
                datos: ""
            })
                .then(function (respuesta) {
                    $scope.producto = respuesta;
                    guardar_componente = false;
                    $scope.buscar_cod_alterno();
                    $scope.buscar_precios();
                }, function () {
                });
        };
        $scope.abrir_catalogo_familia = function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de familias",
                template: "views/msjBox/msjBoxCatFamilias.html",
                controlador: "CtrlCatFamilias",
                datos: 'COMPONENTE'
            })
                .then(function (respuesta) {
                    $scope.producto.cvefamilia = respuesta.codigo;
                    $scope.producto.familia = respuesta.descripcion;
                }, function () {
                });
        };
        $scope.abrir_catalogo_unidad = function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de unidad de medida",
                template: "views/msjBox/msjBoxCatalogos.html",
                controlador: "CtrlCatalogos",
                datos: {
                    grp: 'CVEUNIMEDIDA'
                }


            })
                .then(function (respuesta) {
                    $scope.producto.cveunimedida = respuesta.codigo;
                    $scope.producto.unidad = respuesta.descripcion;
                }, function () {
                });
        };
        $scope.limpiar = function () {
            $scope.request = {};
            $scope.producto = {};
            guardar_componente = true;
            $scope.producto.alterno = {};
            $scope.producto.alterno.mostrar = '0';
            $scope.lista_precios = '';
            $scope.datos_lista_alterno = '';
        };

        //GUARDAMOS O EDITAMOS COMPONENTE
        $scope.generar_folio = function () {
            $scope.request.tipo = "request";
            $scope.request.cmdj = "crear secuencia where tipo = 'COMPONENTE'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_codigo) {
                    $scope.producto.codigo = data_codigo.rows[0].numsecuencia;

                });
        };
        $scope.busqueda_familias = function () {
            $scope.request = {};
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar catalogo where grp = 'CVEFAMILIA' and subgrp = 'COMPONENTE'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_busqueda_familias) {
                    $scope.lista_familias = data_busqueda_familias.rows;
                    // console.info(data.data.rows[0].msj);

                });
        };
        $scope.busqueda_unidad = function () {
            $scope.request = {};
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar catalogo where grp = 'CVEUNIMEDIDA'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_busqueda_unidades) {
                    $scope.lista_unidad = data_busqueda_unidades.rows;
                });
        };
        $scope.guardar = function () {
            $scope.boton_guardar_producto.cargando_boton = true;
            if (guardar_componente) {
                $scope.guardar_componente();
            } else {
                $scope.editar_componente();
            }

        };
        $scope.guardar_componente = function () {
            if ($scope.producto.codigo == "" || $scope.producto.codigo == undefined || $scope.producto.descripcion == "" || $scope.producto.descripcion == undefined || $scope.producto.cvefamilia == "" || $scope.producto.cvefamilia == undefined || $scope.producto.cveunimedida == "" || $scope.producto.cveunimedida == undefined) {
                Notificaciones.notificacion("ERROR", "Debe llenar todos los datos", "error", true);
                $scope.boton_guardar_producto.cargando_boton = false;
                return;
            };
            $scope.request.tipo = "request";
            $scope.request.cmdj = "crear componente where componente = '" + $scope.producto.codigo + "' and cvefamilia = '" + $scope.producto.cvefamilia + "' and cveunimedida = '" + $scope.producto.cveunimedida + "' and  descripcion = '" + $scope.producto.descripcion + "' and desc_corta = '' ";
            PeticionHttp.getAll($scope.request)
                .then(function (data_guardar_cabecera) {
                    //$scope.limpiar_busqueda_directa();
                    /* $scope.buscar_cod_alterno($scope.producto_seleccionado.codigo);
                     $scope.buscar_precios($scope.producto_seleccionado.codigo);
                     $scope.ocultarprecios = false;
                     */
                    guardar_componente = false;
                    Notificaciones.notificacion("CORRECTO", data_guardar_cabecera.rows[0].msj, "success", true);
                    $scope.boton_guardar_producto.cargando_boton = false;

                },function(){
                    $scope.boton_guardar_producto.cargando_boton = false;
                });

        };
        $scope.editar_componente = function () {
            $scope.request.tipo = "request";
            $scope.request.cmdj = "actualizar componente where componente = '" + $scope.producto.codigo + "' and cvefamilia = '" + $scope.producto.cvefamilia + "' and cveunimedida = '" + $scope.producto.cveunimedida + "' and  descripcion = '" + $scope.producto.descripcion + "' and desc_corta = '' ";
            PeticionHttp.getAll($scope.request)
                .then(function (data_editar_cabecera) {
                    guardar_componente = false;
                    Notificaciones.notificacion("CORRECTO", data_editar_cabecera.rows[0].msj, "success", true);
                    $scope.boton_guardar_producto.cargando_boton = false;
                },function(){
                    $scope.boton_guardar_producto.cargando_boton = false;
                });
        };
        $scope.eliminar_componente = function () {
            msgBox.popUpOk({
                mensaje: "¿Confirma que desea eliminar el componente?",
                titulo: "Confirmacion",
                template: "views/msjBox/msjBoxConfirmacion.html",
                controlador: "CtrlConfirmacion",
                datos: ''
            }).then(function (respuesta) {
            $scope.request.tipo = "request";
            $scope.request.cmdj = "eliminar_componente where codigo = '" + $scope.producto.codigo + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_eliminar_cabecera) {
                    guardar_componente = true;
                    Notificaciones.notificacion("CORRECTO", data_eliminar_cabecera.rows[0].msj, "success", true);
                    $scope.limpiar();

                });
            }, function () {
            });
        };
        //TERMINNA GUARDADO Y EDICION DE COMPONENTE

        ////CODIGO ALTERNO
        $scope.busqueda_tipos_alt = function () {
            $scope.request = {};
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar codigos where grp = 'TIPOCODIGOALT'  and estado = 'A' ";
            PeticionHttp.getAll($scope.request)
                .then(function (data_tipos_alt) {
                    $scope.lista_tipos_alt = data_tipos_alt.rows;
                });

        };
        $scope.guardar_alt = function () {
            $scope.boton_guardar_alt.cargando_boton = true;
            if (guardar_cod_alterno) {
                $scope.confirmar_guardar_alt();
            } else {
                $scope.confirmar_edicion_alt();
            }

        };
        $scope.confirmar_guardar_alt = function () {
            if ($scope.producto.alterno.codigoalt == "" || $scope.producto.alterno.codigoalt == undefined || $scope.producto.alterno.tipocodigoalt == "" || $scope.producto.alterno.tipocodigoalt == undefined || $scope.producto.alterno.mostrar == "" || $scope.producto.alterno.mostrar == undefined) {
                Notificaciones.notificacion("ERROR", "Debe ingresar un codigo alterno", "error", true);
                return;
            };
            $scope.request.cmdj = "crear codigo alterno where codigo = '" + $scope.producto.codigo + "' and codigoalt = '" + $scope.producto.alterno.codigoalt + "' and tipo = '" + $scope.producto.alterno.tipocodigoalt + "' and mostrar = '" + $scope.producto.alterno.mostrar + "'  ";
            PeticionHttp.getAll($scope.request)
                .then(function (data_guardar_alterno) {
                    Notificaciones.notificacion("CORRECTO", data_guardar_alterno.rows[0].msj, "success", true);
                    $scope.buscar_cod_alterno();
                    $scope.limpiar_alt();
                    $scope.boton_guardar_alt.cargando_boton = false;
                }, function(){
                    $scope.boton_guardar_alt.cargando_boton = false;
                });
        };
        $scope.confirmar_edicion_alt = function () {
            $scope.request.cmdj = "actualizar codigo alterno where  codigo = '" + $scope.producto.alterno.codigo + "' and codigoalt  = '" + $scope.producto.alterno.codigoalt + "' and  tipo = '" + $scope.producto.alterno.tipocodigoalt + "' and mostrar = '" + $scope.producto.alterno.mostrar + "' ";
            PeticionHttp.getAll($scope.request)
                .then(function (data_guardar_cambio_alt) {
                    Notificaciones.notificacion("CORRECTO", data_guardar_cambio_alt.rows[0].msj, "success", true);
                    $scope.buscar_cod_alterno();
                    $scope.limpiar_alt();
                    $scope.boton_guardar_alt.cargando_boton = false;
                },function(){
                    $scope.boton_guardar_alt.cargando_boton = false;
                });
        };
        $scope.buscar_cod_alterno = function () {
            $scope.request = {};
            $scope.request.cmdj = "listar codigo alterno where codigo = '" + $scope.producto.codigo + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_buscar_codalt) {
                    $scope.datos_lista_alterno = data_buscar_codalt.rows;
                    $scope.cargando = false;
                });

        };
        $scope.limpiar_alt = function () {
            $scope.producto.alterno = {};
            guardar_cod_alterno = true;
        };
        ///TERMINA CODIGO ALTERNO

        //PRECIOS
        $scope.lista_tipo_rem = function () {
            $scope.request = {};
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar catalogo where grp = 'TIPO-PRECIO'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_busqueda_tipo_rem) {
                    $scope.lista_tipo_precio = data_busqueda_tipo_rem.rows;
                });
        };
        $scope.guardar_precios = function () {
            $scope.boton_guardar_precio.cargando_boton = true;
            if (guardar_precios) {
                $scope.confirmar_guardar_precios();
            } else {
                $scope.confirmar_edicion_precios();
            }

        };
        $scope.confirmar_guardar_precios = function () {
            $scope.request.cmdj = "crear precio where tipo = '" + $scope.producto.precios.tipoprecio + "' and codigo = '" + $scope.producto.codigo + "' and precio = '" + $scope.producto.precios.precio + "' and unipza= '" + $scope.producto.precios.unixpza + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_guardar_precio) {
                    Notificaciones.notificacion("CORRECTO", data_guardar_precio.rows[0].msj, "success", true);
                    $scope.buscar_precios();
                    $scope.limpiar_precios();
                    $scope.boton_guardar_precio.cargando_boton = false;
                }, function(){
                    $scope.boton_guardar_precio.cargando_boton = false;
                });
        };
        $scope.confirmar_edicion_precios = function () {
            $scope.request.cmdj = "actualizar precio where tipo = '" + $scope.producto.precios.tipoprecio + "' and codigo = '" + $scope.producto.codigo + "' and precio = '" + $scope.producto.precios.precio + "' and unixpza= '" + $scope.producto.precios.unixpza + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_editar_precio) {
                    Notificaciones.notificacion("CORRECTO", data_editar_precio.rows[0].msj, "success", true);
                    $scope.buscar_precios();
                    $scope.limpiar_precios();
                    $scope.boton_guardar_precio.cargando_boton = false;
                },function(){
                    $scope.boton_guardar_precio.cargando_boton = false;
                });
        };
        $scope.eliminar_precio = function () {
            $scope.boton_eliminar_precio.cargando_boton = true
            $scope.request.tipo = "request";
            $scope.request.cmdj = "eliminar precio where codigo = '" + $scope.producto.codigo + "' and tipoprecio = '" + $scope.producto.precios.tipoprecio + "' ";
            PeticionHttp.getAll($scope.request)
                .then(function (data_eliminar_precio) {
                    Notificaciones.notificacion("CORRECTO", data_eliminar_precio.rows[0].msj, "success", true);
                    $scope.buscar_precios();
                    $scope.producto.precios = {};
                    guardar_precios = true;
                    $scope.boton_eliminar_precio.cargando_boton = false;
                });

        };
        $scope.buscar_precios = function () {
            $scope.request = {};
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar precio where codigo = '" + $scope.producto.codigo + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_busqueda_precios) {
                    $scope.lista_precios = data_busqueda_precios.rows;
                    $scope.num_registros = $scope.lista_precios.length;
                    $scope.num_paginas = (Math.ceil($scope.lista_precios.length / 10));
                    $scope.cargando = false;

                });

        };
        $scope.limpiar_precios = function(){
            $scope.producto.precios = {};
            guardar_precios = true;
        };
        //TERMINA PRECIOS

        //SUBIR IMAGEN
        $scope.submit = function () {
            if ($scope.form.file.$valid && $scope.file) {
                $scope.upload($scope.file, $scope.producto.codigo);
            }
        };

        // upload on file select or drop
        $scope.upload = function (file, codigo_img) {
            console.info(file);
            Upload.upload({
                url: '?action=subir_imagen',
                file: file,
                //data: {file: {name: $scope.producto_seleccionado.codigo+'.jpg'}, 'username': $scope.username}
                data: { 'codigo_img': codigo_img }
            }).then(function (resp) {
                console.log('Success ' + resp.config.data.file.name + ' uploaded. Response: ' + resp.data);

                var random = (new Date()).toString();
                $scope.imagen = "res/imagenes/productos/" + $scope.producto_seleccionado.codigo + ".jpg?rnd=" + random;
                console.info($scope.imagen);

            }, function (resp) {
                console.error('Error status: ' + resp.status);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        };

        //TERMINA SUBIDA DE IMAGEN

        $scope.busqueda_familias();
        $scope.busqueda_tipos_alt();
        $scope.busqueda_unidad();
        $scope.lista_tipo_rem();

    });