angular.module('webapp')
    .controller('CtrlActualizacionPrecios', function ($scope, msgBox, PeticionHttp, Notificaciones, $uibModal, $rootScope, $mdDialog, Upload) {
        $scope.titulo = 'Actualizacion de precios';
        $scope.request = {};
        $scope.actualizacion = {};
        Notificaciones.banderaNotificacion = true;

        /////CONSFIGURACIONES
        $scope.tabla = {
            nocolapsado: [
                {
                    alias: 'CODIGO',
                    campo: 'codigo',
                    tamano: '2'
                },
                {
                    alias: 'CODIGO ALTERNO',
                    campo: 'mostrarcodigo',
                    tamano: '2'
                },
                {
                    alias: 'DESCRIPCION',
                    campo: 'descripcion',
                    tamano: '3'
                },
                {
                    alias: 'FAMILIA',
                    campo: 'familia',
                    tamano: '3'
                },
                {
                    alias: 'UNIDAD',
                    campo: 'unidad',
                    tamano: '2'
                }
            ],
            colapsado: ['codigo', 'descripcion', 'familia', 'unidad'],
            colapsarsiempre: false,
            func: function(registro){
                pasoInfo.data = registro;
                $location.url("/consulta_remision");
            }
        };
        $scope.txtdescfamilia = {
            txt: {
                placeholder: 'Seleccione una familia',
                label: 'Descripcion familia',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: true,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtprecio = {
            txt: {
                placeholder: 'Ingrese un precio o porcentaje',
                label: 'Precio/porcentaje',
                icono: 'fa-money',
                password: false,
                disabled: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txttipoprecios = {
            txt: {
                placeholder: 'Seleccione un tipo de precio',
                label: 'Tipo precio',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: true,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        //CONFIGURACIONES
        $scope.abrir_catalogo_familia = function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de familias",
                template: "views/msjBox/msjBoxCatFamilias.html",
                controlador: "CtrlCatFamilias",
                datos: 'COMPONENTE'
            })
                .then(function (respuesta) {
                    $scope.actualizacion.cvefamilia = respuesta.codigo;
                    $scope.actualizacion.familia = respuesta.descripcion;
                    $scope.buscar_componentes();
                }, function () {
                });
        };
        $scope.abrir_catalogo_tipo_precios= function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de tipo de precios",
                template: "views/msjBox/msjBoxCatalogos.html",
                controlador: "CtrlCatalogos",
                datos: {
                    grp: 'TIPO-PRECIO'
                }


            })
                .then(function (respuesta) {
                    $scope.actualizacion.tipoprecio = respuesta.codigo;
                    $scope.actualizacion.tipopreciodesc = respuesta.descripcion;
                }, function () {
                });
        };
        $scope.busqueda_familias = function () {
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar catalogo where grp = 'CVEFAMILIA' and subgrp = 'COMPONENTE'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_busqueda_familias) {
                    $scope.lista_familias = data_busqueda_familias.rows;
                });


        };
        $scope.lista_tipo_rem = function () {
            $scope.request = {};
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar catalogo where grp = 'TIPO-PRECIO'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_busqueda_tipo_rem) {
                    $scope.lista_tipo_precio = data_busqueda_tipo_rem.rows;
                });
        };
        $scope.buscar_componentes = function () {
            $scope.cargando = true;
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar componentes where cvefamilia = '" + $scope.actualizacion.cvefamilia + "' and estado = 'A'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_comp_familia) {
                    $scope.datos_lista = data_comp_familia.rows;
                    $scope.num_registros = $scope.datos_lista.length;
                    $scope.num_paginas = (Math.ceil($scope.datos_lista.length / 10));
                    $scope.cargando = false;
                });
        };
        $scope.limpiar = function () {
            $scope.actualizacion = {};
            $scope.datos_lista = '';
        };
        $scope.guardar = function () {
            $scope.cargando = true;
            if ($scope.actualizacion.tipo_cambio == 'POR') {
                $scope.actualizacion.precio = $scope.actualizacion.precio / 100;

                if ($scope.actualizacion.precio == '1') {
                    $scope.actualizacion.precio = parseFloat($scope.actualizacion.precio).toFixed(2);
                }
            };
            if ($scope.tipo_cambio == 'FIJO') {
                $scope.actualizacion.precio = parseFloat($scope.actualizacion.precio).toFixed(2);
            };
            $scope.actualizacion.precio = parseFloat($scope.actualizacion.precio).toFixed(2);

            $scope.request.tipo = "request";
            $scope.request.cmdj = "actualizar precio masivo where tipo = '" + $scope.actualizacion.tipo_cambio + "' and cvefamilia = '" + $scope.actualizacion.cvefamilia + "' and valor = " + $scope.actualizacion.precio + " and  tipoprecio = '" + $scope.actualizacion.tipoprecio + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_guardar) {
                    Notificaciones.notificacion("CORRECTO", data_guardar.rows[0].msj, "success", true);
                    $scope.cargando = false;
                    $scope.limpiar();

                },function(){
                    $scope.cargando = false;
                });
        };
       
        $scope.busqueda_familias();
        $scope.lista_tipo_rem();
    });