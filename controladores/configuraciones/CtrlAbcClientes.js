angular.module('webapp')
    .controller('CtrlAbcClientes', function ($scope, msgBox, PeticionHttp, Notificaciones, $uibModal, $rootScope, $mdDialog, Upload) {

        $scope.titulo = 'ABC Clientes';
        $scope.cliente = {};
        var guardar = true;
        $scope.mostrar_codigo = false;
        $scope.request = {};
        Notificaciones.banderaNotificacion = true;

        // CATALOGO
        $scope.abrir_catalogo = function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de clientes",
                template: "views/msjBox/msjBoxCatClientes.html",
                controlador: "CtrlCatClientes",
                datos: ''
            }).then(function (respuesta) {
                $scope.cliente = respuesta;
                guardar = false;
                $scope.mostrar_codigo = true;

            }, function () {
            });
        };
        // TEMINA CATALOGO

        $scope.limpiar = function () {
            $scope.cliente = {};
            $scope.mostrar_codigo = false;
            guardar = true;
        };
        $scope.lista_tipo_remisiones = function () {
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar codigos where grp = 'TIPOREM'";
            PeticionHttp.getAll($scope.request)
                .then(function (data) {
                    $scope.datos_lista_tipo_rem = data.rows;
                    $scope.cargando_tipo = false;
                });
        };

        //ABC 
        $scope.guardar = function () {
            if (guardar) {
                $scope.guardar_cliente();
            } else {
                $scope.editar_cliente();
            }

        };
        $scope.guardar_cliente = function () {
            $scope.cargando = true;
            if ($scope.cliente.codigo_cliente == "" || $scope.cliente.codigo_cliente == undefined) {
                Notificaciones.notificacion("ERROR", "Debe ingresar un codigo de cliente", "error", true);
                $scope.cargando = false;
                return;
            };
            $scope.request.tipo = "request";
            $scope.request.cmdj = "crear cliente  where codigo_cliente = '" + $scope.cliente.codigo_cliente + "' and nombre = '" + $scope.cliente.nombre + "' and telefono = '" + $scope.cliente.telefono + "' and	fecha_creacion = '" + $scope.cliente.fecha_creacion + "' and	correo_electronico = '" + $scope.cliente.correo_electronico + "' and estado = '" + $scope.cliente.estado + "' and	ciudad = '" + $scope.cliente.ciudad + "' and	municipio_delegacion = '" + $scope.cliente.municipio_delegacion + "' and	calle = '" + $scope.cliente.calle + "' and	num_interior = '" + $scope.cliente.num_interior + "' and	cp = '" + $scope.cliente.cp + "' and	nomcontacto = '" + $scope.cliente.nomcontacto + "' and	numexterior = '" + $scope.cliente.numexterior + "' and	tipocliente = '" + $scope.cliente.tipocliente + "' and	subtipocliente = '' and	direccion = '" + $scope.cliente.direccion + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_guardar_cliente) {
                    Notificaciones.notificacion("CORRECTO", data_guardar_cliente.rows[0].msj, "success", true);
                    guardar = true;
                    $scope.cargando = false;
                    $scope.limpiar();
                },function(){
                    $scope.cargando = false;
                });

        };
        $scope.editar_cliente = function () {
            $scope.cargando = true;
            $scope.request.tipo = "request";
            $scope.request.cmdj = "actualizar cliente  where codigo_cliente = '" + $scope.cliente.codigo_cliente + "' and nombre = '" + $scope.cliente.nombre + "' and telefono = '" + $scope.cliente.telefono + "' and	fecha_creacion = '" + $scope.cliente.fecha_creacion + "' and	correo_electronico = '" + $scope.cliente.correo_electronico + "' and estado = '" + $scope.cliente.estado + "' and	ciudad = '" + $scope.cliente.ciudad + "' and	municipio_delegacion = '" + $scope.cliente.municipio_delegacion + "' and	calle = '" + $scope.cliente.calle + "' and	num_interior = '" + $scope.cliente.num_interior + "' and	cp = '" + $scope.cliente.cp + "' and	nomcontacto = '" + $scope.cliente.nomcontacto + "' and	numexterior = '" + $scope.cliente.numexterior + "' and	tipocliente = '" + $scope.cliente.tipocliente + "' and	subtipocliente = '' and	direccion = '" + $scope.cliente.direccion + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_editar_cliente) {
                    Notificaciones.notificacion("CORRECTO", data_editar_cliente.rows[0].msj, "success", true);
                    guardar = true;
                    $scope.cargando = false;
                    $scope.limpiar();
                },function(){
                    $scope.cargando = false;
                });
        };
        $scope.eliminar_cliente = function () {
            $scope.request.tipo = "request";
            $scope.request.cmdj = "eliminar cliente  where codigo_cliente = '" + $scope.cliente.codigo_cliente + "' ";
            PeticionHttp.getAll($scope.request)
                .then(function (data_eliminar_cliente) {
                    Notificaciones.notificacion("CORRECTO", data_eliminar_cliente.rows[0].msj, "success", true);
                    guardar = true;
                    $scope.limpiar();
                });
        };
        // TERMINA ABC 

        $scope.lista_tipo_remisiones();
    });