angular.module('webapp')
    .controller('CtrlListaRemision', function ($scope, msgBox, PeticionHttp, $location, pasoInfo) {

        $scope.request = {};
        $scope.cargando = false;

        //  TABLAS  

        $scope.tabla = {
            nocolapsado: [
                {
                    alias: 'ID',
                    campo: 'remision',
                    tamano: '2'
                },
                {
                    alias: 'Tipo',
                    campo: 'codigo',
                    tamano: '2'
                },
                {
                    alias: 'Cliente',
                    campo: 'cliente',
                    tamano: '2'
                },
                {
                    alias: '# Productos',
                    campo: 'productos',
                    tamano: '1'
                },
                {
                    alias: 'Total',
                    campo: 'total',
                    tamano: '1'
                },
                {
                    alias: 'Fecha creacion',
                    campo: 'fhcrea',
                    tamano: '2'
                },
                {
                    alias: 'Usuario creacion',
                    campo: 'ucrea',
                    tamano: '2'
                },
            ],
            colapsado: ['remision', 'codigo', 'cantidad', 'total'],
            colapsarsiempre: false,
            func: function(registro){
                pasoInfo.data = registro;
                $location.url("/consulta_remision");
            }
        };
        // END TABLAS
        $scope.lista_remisiones = function () {
            $scope.cargando = true;
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar remisiones";
            PeticionHttp.getAll($scope.request)
            .then(function (data) {
                $scope.datos_lista = data.rows;
                $scope.num_registros = $scope.datos_lista.length;
                $scope.num_paginas = (Math.ceil($scope.datos_lista.length / 10));
                $scope.cargando = false;

                });
        };

        $scope.seleccionar = function(registro, index){
            pasoInfo.data = registro;
            $location.url("/consulta_remision");
        };
        $scope.lista_remisiones();
    });