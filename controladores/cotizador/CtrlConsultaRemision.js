angular.module('webapp')
    .controller('CtrlConsultaRemision', function ($scope, msgBox, PeticionHttp, $location, pasoInfo) {
        $scope.request = {};
        $scope.titulo = 'Consulta remision';
        $scope.cargando_cabecera = false;
        $scope.cargando_lineas = false;

        $scope.tabla = {
            nocolapsado: [
                {
                    alias: 'ID',
                    campo: 'remision',
                    tamano: '1'
                },
                {
                    alias: 'LINEA DE REMISION',
                    campo: 'linremision',
                    tamano: '2'
                },
                {
                    alias: 'CODIGO',
                    campo: 'codigo',
                    tamano: '1'
                },
                {
                    alias: 'DESCRIPCION',
                    campo: 'descripcion',
                    tamano: '3'
                },
                {
                    alias: 'CANTIDAD',
                    campo: 'cantidad',
                    tamano: '1'
                },
                {
                    alias: 'PRECIO PZA',
                    campo: 'preciopza',
                    tamano: '1'
                },
                {
                    alias: 'PRECIO TOTAL',
                    campo: 'preciototal',
                    tamano: '1'
                },
                {
                    alias: 'USUARIO',
                    campo: 'ucrea',
                    tamano: '1'
                },
                {
                    alias: 'FECHA CREACION',
                    campo: 'fhcrea',
                    tamano: '1'
                }
            ],
            colapsado: ['remision', 'codigo', 'cliente', 'total'],
            func: function(registro){
                pasoInfo.data = registro;
                $location.url("/consulta_remision");
            }
        };

        $scope.cabecera_remision = function () {
            $scope.cargando_cabecera = true;
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar remision where remision = '" + pasoInfo.data.remision + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data) {
                    $scope.datos_cabecera = data.rows[0];
                    $scope.cargando_cabecera = false;
                    $scope.lineas_remision();
                });
        };

        $scope.lineas_remision = function () {
            $scope.cargando_lineas = true;
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar lineas remision where remision = '" + pasoInfo.data.remision + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data) {
                    $scope.datos_lista = data.rows;
                    console.info(data.rows);
                    $scope.cargando_lineas = false;
                    $scope.num_registros = $scope.datos_lista.length;
                    $scope.num_paginas = (Math.ceil($scope.datos_lista.length / 10));
                    $scope.isRouteLoading = false;

                });
        };

        //  TABLAS  
        $scope.pagina_numero = {};
        $scope.pagina_act = 1;
        $scope.pagina_numero.pagina = 1;
        $scope.anterior = false;
        $scope.siguiente = true;

        $scope.rest_pagina_act = function () {
            $scope.pagina_act = 1;
        }

        $scope.sumar_pag = function () {
            $scope.pagina_act = parseInt($scope.pagina_act) + 1;
            $scope.pagina_numero.pagina = $scope.pagina_act;
            if ($scope.pagina_act == 0) {
                $scope.pagina_act = 1;
                $scope.pagina_numero.pagina = $scope.pagina_act;
            }
            $scope.cambiar_pag();
        };
        $scope.res_pag = function () {
            $scope.pagina_act = parseInt($scope.pagina_act) - 1;
            $scope.pagina_numero.pagina = $scope.pagina_act;
            if ($scope.pagina_act > $scope.num_paginas) {
                $scope.pagina_act = $scope.num_paginas;
                $scope.pagina_numero.pagina = $scope.pagina_act;
            }
            $scope.cambiar_pag();
        };

        $scope.cambiar_pag = function () {
            if ($scope.pagina_numero.pagina == '' || $scope.pagina_numero.pagina == undefined || $scope.pagina_numero.pagina == 0) {
                $scope.pagina_act = 1;
            } else {
                $scope.pagina_act = parseInt($scope.pagina_numero.pagina);
                if ($scope.pagina_act > $scope.num_paginas) {
                    $scope.pagina_act = $scope.num_paginas;
                    $scope.pagina_numero.pagina = $scope.num_paginas;
                }
            }

        };

        // END TABLAS
        $scope.cabecera_remision();
    });