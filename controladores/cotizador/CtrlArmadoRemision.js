angular.module('webapp')
    .controller('CtrlArmadoRemision', function ($scope, msgBox, PeticionHttp, Notificaciones, $rootScope) {

        
        $scope.titulo = 'Armado de nota de remision';
        $scope.lista_agregados = [];
        $scope.request = {};
        $scope.remision = {};
        $scope.producto_seleccionado = {};
        $scope.remision.cantidad = 0;
        $scope.remision.incremento = 0;
        $scope.remision.subtotal = 0;
        $scope.remision.total = 0;
        $scope.remision.cant_desc = 0;
        $scope.remision.precio_minimo = 0;
        $scope.remision.cantidad_iva = 0;
        $scope.btarticulo = false;
        $scope.btcat = false;
        $scope.habilitarbtn = true;
        $scope.habilitarcborem = false;
        $scope.habilitarimp = true;
        $scope.habilitargrd = true;
        $scope.habilitarir = true;
        $scope.cargando_tipo = true;
        $scope.cargando_cliente = false;
        $scope.cargando_producto = false;
        $scope.cargando_guardar_remision = false;

        ///CONFIGURACIONES
        $scope.txtcodigoremision = {
            txt: {
                placeholder: '',
                label: 'Codigo tipo de remision',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: true,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtdescremision = {
            txt: {
                placeholder: '',
                label: 'Descripcion',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtcodigocliente = {
            txt: {
                placeholder: '',
                label: 'Codigo cliente',
                icono: 'fa-file-text',
                password: false,
                disabled: true
            },
            boton: {
                encendido: true,
                iconoBoton: 'fa-list-alt',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.tabla = {
            nocolapsado: [
                {
                    alias: 'CANTIDAD',
                    campo: 'cantidad',
                    tamano: '1'
                },
                {
                    alias: 'CODIGO',
                    campo: 'codigo',
                    tamano: '2'
                },
                {
                    alias: 'DESCRIPCION',
                    campo: 'descripcion',
                    tamano: '5'
                },
                {
                    alias: 'PRECIO',
                    campo: 'precio',
                    tamano: '2'
                },
                {
                    alias: 'IMPORTE',
                    campo: 'importe',
                    tamano: '2'
                }
            ],
            colapsado: ['cantidad', 'descripcion', 'codigo', 'precio', 'importe'],
            colapsarsiempre: false,
            func: function (registro, index) {
                $scope.lista_agregados.splice(index, 1);
                if ($scope.lista_agregados.length <= 0) {
                    $scope.habilitarcborem = false;
                    $scope.habilitargrd = true;
                }
                $scope.calculo_subtotal();
            }
        };
        $scope.txtnombrecliente = {
            txt: {
                placeholder: 'Nombre cliente',
                label: 'Nombre cliente',
                icono: ' fa-user',
                password: false,
                disabled: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-flash',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtclientedireccion = {
            txt: {
                placeholder: 'Direccion',
                label: 'Direccion',
                icono: 'fa-map',
                password: false,
                disabled: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-flash',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.txtclientetelefono = {
            txt: {
                placeholder: 'Telefono',
                label: 'Telefono',
                icono: 'fa-phone-square',
                password: false,
                disabled: false
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-flash',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.boton_limpiar_cliente = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Limpiar',
            cargando_boton: false,
            icono: 'fa-eraser',
            datos: {}
        };
        $scope.boton_aplicar_descuento = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Aplicar aumento',
            cargando_boton: false,
            icono: 'fa-check-circle-o',
            datos: {}
        };
        $scope.txtincremento = {
            txt: {
                placeholder: '% incremento',
                label: '% incremento',
                icono: 'fa-map',
                password: false,
                disabled: false
            },
            boton: {
                encendido: true,
                iconoBoton: 'fa-eraser',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.boton_agregar = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Agregar',
            cargando_boton: false,
            icono: 'fa-plus',
            datos: {}
        };
        $scope.boton_limpiar_producto = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Limpiar producto',
            cargando_boton: false,
            icono: 'fa-eraser',
            datos: {}
        };
        $scope.txtdescuento = {
            txt: {
                placeholder: '% descuento',
                label: '% descuento',
                icono: 'fa-arrow-down',
                password: false,
                disabled: false
            },
            boton: {
                encendido: true,
                iconoBoton: 'fa-eraser',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.boton_aplicar_descuento = {
            color: $scope.color_elementos,
            tamano: 'sm',
            texto: 'Aplicar descuento',
            cargando_boton: false,
            icono: 'fa-check-circle-o',
            datos: {}
        };
        
        /// END CONFIGURACIONES
        //NOTIFICACIONES
        Notificaciones.banderaNotificacion = true;
        // END NOTIFICACIONES
        $scope.abrir_catalogo_tipo_remision = function () {
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de tipo de remision",
                template: "views/msjBox/msjBoxCatalogos.html",
                controlador: "CtrlCatalogos",
                datos: {
                    grp: 'TIPOREM'
                }
            })
                .then(function (respuesta) {
                    $scope.remision.tipo_remision = respuesta.codigo;
                    $scope.remision.desc_tipo_remision = respuesta.descripcion;
                    $scope.lista_clientes();
                }, function () {
                });
        };
        $scope.abrir_catalogo_clientes = function () {
            if($scope.remision.tipo_remision == '' || $scope.remision.tipo_remision == undefined){
                Notificaciones.notificacion("ERROR", "Debe seleccionar un tipo de remision", "error", true);
                return;
            };
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo clientes de tipo de remision: " + $scope.remision.desc_tipo_remision +"",
                template: "views/msjBox/msjBoxCatClientes.html",
                controlador: "CtrlCatClientes",
                datos: $scope.remision.tipo_remision
            })
                .then(function (respuesta) {
                    $scope.remision.cliente = respuesta;
                    $scope.lista_clientes();
                }, function () {
                });
        };
        $scope.lista_tipo_remisiones = function () {
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar codigos where grp = 'TIPOREM'";
            PeticionHttp.getAll($scope.request)
                .then(function (data) {
                    $scope.datos_lista_tipo_rem = data.rows;
                    $scope.cargando_tipo = false;
                });
        };
        $scope.lista_clientes = function () {
            $scope.cargando_cliente = true;
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar clientes where tipocliente = '" + $scope.remision.tipo_remision + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_clientes) {
                    $scope.datos_lista_clientes = data_clientes.rows;
                    $scope.cargando_cliente = false;
                });
        };
        $scope.limpiar_cliente = function () {
            $scope.remision.cliente = '';
        };
        $scope.limpiar_producto = function () {
            $scope.remision.producto = {};
            $scope.remision.cantidad = '';
        }
        $scope.busqueda_directa = function () {
            $scope.cargando_producto = true;
            if ($scope.remision.tipo_remision == undefined || $scope.remision.tipo_remision == '') {
                Notificaciones.notificacion("ERROR", "Debe ingresar el tipo de remision", "error", true);

                return;
            };
            $scope.request.archivo = 'arch_prueba';
            $scope.request.rastreo = true;

            $scope.request.cmdj = "listar producto where producto = '" + $scope.remision.bus_directa + "' and tiporem = '" + $scope.remision.tipo_remision + "' and estado = 'A'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_busqueda_directa) {
                    $scope.remision.producto = data_busqueda_directa.rows[0];
                    $scope.remision.precio_unitario_original = $scope.remision.producto.precio;
                    $scope.remision.incremento = 0;
                    $scope.remision.cantidad = 0;
                    $scope.cargando_producto = false;
                });

            $scope.cargando_producto = false;
            $scope.bus_directa = "";
            $('#cantidad').focus();
        };
        $scope.abrir_catalogo = function () {
            if ($scope.remision.tipo_remision == undefined || $scope.remision.tipo_remision == '') {
                Notificaciones.notificacion("ERROR", "Debe seleccionar un tipo de remision", "error", true);
                return;

            }
            msgBox.popUpOk({
                mensaje: "Esto es el cuerpo del mensaje",
                titulo: "Catalogo de productos",
                template: "views/msjBox/msjBoxCatProductos.html",
                controlador: "CtrlCatProductos",
                datos: $scope.remision.tipo_remision
            })
                .then(function (respuesta) {
                    $scope.remision.producto = respuesta;
                    $scope.remision.precio_unitario_original = $scope.remision.producto.precio;
                    $scope.remision.incremento = 0;
                    $scope.remision.cantidad = 0;
                    $scope.cargando_producto = false;
                    $scope.foco_cantidad = true;

                }, function () {
                });
        };
        //CALCULO IMPORTE TOTAL
        $scope.calculo_importe_cantidad = function () {
            if ($scope.remision.cantidad > 0) {
                $scope.habilitarbtn = false;
            } else {
                $scope.habilitarbtn = true;
            }

            $scope.remision.importe = $scope.remision.producto.precio * $scope.remision.cantidad;
            //$scope.producto_seleccionado.importe = $scope.producto_seleccionado.importe.toFixed(2);
            $scope.remision.importe_original = $scope.remision.precio_unitario_original * $scope.remision.cantidad;

        };
        //BOTON DE CALCULO DE INCREMENTO Y DE LIMPIADO DE INCRMEENTO
        $scope.calculo_precio_unitario_incremento = function () {

            uno_porciento = $scope.remision.precio_unitario_original / 100;

            cantidad_a_incrementar = $scope.remision.incremento * uno_porciento;
            $scope.remision.producto.precio = parseFloat($scope.remision.precio_unitario_original) + parseFloat(cantidad_a_incrementar);
            $scope.calculo_importe_cantidad();
        };
        $scope.limpiar_incremento = function () {
            $scope.remision.producto.precio = $scope.remision.precio_unitario_original;
            $scope.remision.incremento = "";
            $scope.calculo_importe_cantidad();


        };
        // BOTONES DE AGREGADO Y DE LIMPIADO DE scope
        $scope.agregar_producto = function () {
            $scope.habilitarcborem = true;
            $scope.habilitarbtn = true;
            $scope.habilitargrd = false;
            //Valida que hayan agregado algun producto
            if ($scope.remision.producto.codigo == " " || $scope.remision.producto.codigo == undefined) {
                //mensaje de alerta
                Notificaciones.notificacion("ERROR", "Debe seleccionar un articulo", "error", true);
                return;
            }
            //Valida que hayan ingresado la cantidad de articulos
            if ($scope.remision.cantidad == " " || $scope.remision.cantidad == undefined) {
                //mensaje de alerta
                Notificaciones.notificacion("ERROR", "Debe ingresar la cantidad de articulos a agregar", "error", true);
                return;
            }

            $scope.producto_seleccionado.cantidad = $scope.remision.cantidad;
            $scope.producto_seleccionado.codigo = $scope.remision.producto.codigo;
            $scope.producto_seleccionado.descripcion = $scope.remision.producto.descripcion;
            $scope.producto_seleccionado.precio = $scope.remision.producto.precio;
            $scope.producto_seleccionado.importe = $scope.remision.importe;
            $scope.producto_seleccionado.importe_original = $scope.remision.importe_original;

            Notificaciones.notificacion("CORRECTO", "Producto agregado", "success", true);
            $scope.lista_agregados.push($scope.producto_seleccionado);
            $scope.limpiar_producto();
            $scope.calculo_subtotal();

        };
        //CALCULO SUBTOTAL Y TOTAL
        $scope.calculo_subtotal = function () {
            $scope.remision.subtotal = 0;
            $scope.remision.total = 0;
            $scope.remision.total_original = 0;
            $scope.remision.precio_minimo = 0;

            for (var i = 0; i <= $scope.lista_agregados.length - 1; i++) {
                $scope.remision.subtotal = $scope.remision.subtotal + $scope.lista_agregados[i].importe;
                $scope.remision.total = $scope.remision.total + $scope.lista_agregados[i].importe;
                $scope.remision.total_original = $scope.remision.total;
                $scope.remision.precio_minimo = $scope.remision.precio_minimo + $scope.lista_agregados[i].importe_original;
                //CALCULO DE PRECIO MINIMO DE VENTA INICIAL
                if (i == $scope.lista_agregados.length - 1) {
                    uno_porciento_pm = $scope.remision.precio_minimo / 100;
                    cantidad_incrementar_pm = 30 * uno_porciento_pm;
                    $scope.remision.precio_minimo = parseFloat(cantidad_incrementar_pm) + parseFloat($scope.remision.precio_minimo);

                }

            };

            $scope.limpiar_producto();


        };
        //CALCULO DE PRECIO MINIMO DE VENTA
        $scope.precio_minimo_venta = function () {
        };
        //CALCULO IMPORTE TOTAL
        $scope.calculo_importe_cantidad = function () {
            if ($scope.remision.cantidad > 0) {
                $scope.habilitarbtn = false;
            } else {
                $scope.habilitarbtn = true;
            }

            $scope.remision.importe = $scope.remision.producto.precio * $scope.remision.cantidad;
            //$scope.producto_seleccionado.importe = $scope.producto_seleccionado.importe.toFixed(2);
            $scope.remision.importe_original = $scope.remision.precio_unitario_original * $scope.remision.cantidad;

        };
        $scope.limpiar_producto = function () {
            $scope.producto_seleccionado = {};
            $scope.remision.producto = {};
            $scope.remision.cantidad = 0;
            $scope.remision.incremento = 0;
            $scope.remision.importe = 0;
            $scope.remision.importe_original = 0;
            $scope.remision.bus_directa = '';
            $scope.foco_busqueda_directa = true;
        };
        //CALCULO DE DESCUENTO
        $scope.calcular_descuento = function () {
            if ($scope.remision.porcentaje_descuento == " " || $scope.remision.porcentaje_descuento == undefined) {
                //mensaje de alerta
                Notificaciones.notificacion("ERROR", "Debe ingresar un porcentaje de descuento", "error", true);
                return;
            };
            if ($scope.iva == 16) {
                new PNotify({
                    title: 'ERROR',
                    text: 'Primero debe ingresar el descuento y posteriormente el IVA',
                    type: 'error',
                    hide: true,
                    styling: 'bootstrap3'
                });
                return;

            };
            uno_p_desc = $scope.remision.subtotal / 100;
            $scope.remision.cant_desc = uno_p_desc * $scope.remision.porcentaje_descuento;
            $scope.remision.total = $scope.remision.subtotal - $scope.remision.cant_desc;
            $scope.remision.total_original = $scope.remision.total;
        };
        //ANULAR DESCUENTO
        $scope.anular_descuento = function () {

            $scope.remision.porcentaje_descuento = 0;
            $scope.remision.cant_desc = 0;
            $scope.calculo_subtotal();


        };
        //CALCULO DE IVA
        $scope.calcular_iva = function () {
            $scope.remision.iva = 16;
            uno_p_iva = $scope.remision.total_original / 100;
            $scope.remision.cantidad_iva = uno_p_iva * 16;
            $scope.remision.total = $scope.remision.cantidad_iva + $scope.remision.total_original;
        };
        //ANULAR IVA
        $scope.anular_iva = function () {
            $scope.remision.iva = 0;
            $scope.remision.cantidad_iva = 0
            $scope.calculo_subtotal();


        };
        //GUARDAR REMISION
        $scope.guardar_remision = function () {
            $scope.cargando = true;

            // console.info($scope.tipo_remision.codigo);
            if ($scope.remision.tipo_remision == "" || $scope.remision.tipo_remision == undefined || $scope.remision.cliente == "" || $scope.remision.cliente == undefined || $scope.remision.subtotal == "" || $scope.remision.subtotal == undefined || $scope.remision.total == "" || $scope.remision.total == undefined) {
                Notificaciones.notificacion("ERROR", "Debe llenar todos los datos de la remision", "error", true);
                $scope.cargando = false;
                return;
            };

            $scope.request.tipo = "request";
            $scope.request.cmdj = "crear remision where  tiporem = '" + $scope.remision.tipo_remision + "' and cvecfinal = '" + $scope.remision.cliente.codigo_cliente + "' and nombre = '" + $scope.remision.cliente.nombre + "' and  direccion = '" + $scope.remision.cliente.direccion + "' and telefono = '" + $scope.remision.cliente.telefono + "' and subtotal = '" + $scope.remision.subtotal + "' and total = '" + $scope.remision.total + "' and iva = '" + $scope.remision.cantidad_iva + "' and descuento = '" + $scope.remision.cant_desc + "'";
            //$scope.request.cmdj = "crear remision where  remision = 'prueba' and tipo = 'tipo' and cvecfinal = 'cvefinal' and nombre = 'nombre' and  direccion = 'direccion' and telefono = 'tel' and subtotal = '3' and total = '4' and iva = '5'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_guardar_cabecera) {
                    $scope.habilitarimp = false;
                    $scope.habilitargrd = true;
                    $scope.btarticulo = true;
                    $scope.btcat = true;
                    $scope.remision.id_remision = data_guardar_cabecera.rows[0].subgrp;
                    $scope.remision.fecha = data_guardar_cabecera.rows[0].fhcrea;
                    $scope.cargando = false;
                    //formateo el total para enviarlo a remision
                    $scope.remision.total_texto = $scope.remision.total.toFixed(2);

                    $scope.guardar_lineas_remision(data_guardar_cabecera.rows[0].remision, $scope.remision.id_remision);


                },function(){
                    $scope.cargando = false;
                });
        };
        $scope.guardar_lineas_remision = function (id_remision, id_cliente) {
            for (var i = 0; i <= $scope.lista_agregados.length - 1; i++) {
                $scope.request.tipo = "request";
                $scope.request.cmdj = "crear linea remision where remision  = '" + id_remision + "'  and producto =  '" + $scope.lista_agregados[i].codigo + "'  and cantidad = '" + $scope.lista_agregados[i].cantidad + "'  and preciouni ='" + $scope.lista_agregados[i].precio_unitario + "' and preciototal='" + $scope.lista_agregados[i].importe + "' ";

                PeticionHttp.getAll($scope.request)
                    .then(function (data_guardar_linea) {

                    });
            };
            cargando_guardar_remision = false;
            Notificaciones.notificacion("CORRECTO", "Se guardo correctamente la remision", "success", true);
            $scope.comenzar_remision();
        };
        //COMENZAR REMISION
        $scope.comenzar_remision = function () {
            $scope.request = {};
            $scope.remision = {};
            $scope.remision.cantidad = 0;
            $scope.remision.incremento = 0;
            $scope.habilitarbtn = true;
            $scope.habilitarimp = true;
            $scope.habilitargrd = true;
            $scope.lista_agregados = [];
            $scope.producto_seleccionado = {};

        };

        $scope.lista_tipo_remisiones();
    });