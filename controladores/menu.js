angular.module("webapp")
    .controller('menu', function ($scope, navegacion, PeticionHttp, $location) {
        $scope.menuTitle = "MENU DE NAVEGACIÓN";
        $scope.menu = navegacion.menu;

        $scope.$on("crearMenu", function (e, respuestaMenu) {
            $scope.menu = respuestaMenu; //Actualizar el menu en la vista
            $scope.elementos = $scope.menu;
            $scope.elementos.forEach(function (menu_elemento) {
               menu_elemento.string1 = JSON.parse(menu_elemento.string1);
            });
            //window.location.href = $scope.pantalla_inicio;
            $scope.pnavegacion = new Tree(null);
            //Se agregan las opciones del array, ***importante tienen que estar ordenadas segun se va construyendo
            $scope.elementos.forEach(function (item) {
                $scope.pnavegacion.addopcion(item);
            });
            
            /*
                Al terminar es necesario moverse al menu inicial para que tenga sentido, 
                de lo contrario te dejará en la última opcion creada y crear confusión
           */
            $scope.pnavegacion.irinicio();
            habilitamenu(); //Esta funcion aplica los efectos al menu

            
            
        });


        
            //Clase de la cual se crearan los elementos
            //console.info("entre a construccion menu");

            class Node {
                constructor(value) {
                    this.nodo = value
                    this.padre = null
                    this.opciones = [];
                }
            }

            //Clase para armar el panel de navegacion
            class Tree {
                constructor() {
                    this.rama = new Node(null);
                    this.titulo = 'Menu navegacion';
                }

                //Agregar opcion ya sea menu o otro tipo
                addopcion(opcion) {
                    var nuevo = new Node(opcion);
                    this.iramenu(opcion.relacion);//busca el menu al que pertenece la opcines, pero si no encuentra nada quedará en la raiz
                    var papa = this.rama;
                    nuevo.padre = papa;
                    this.rama.opciones.push(nuevo);
                    if (opcion.tipo == "menu") {
                        this.rama = nuevo;
                    }
                }

                //Metodo para ir al inicio del arbol una vez construido el menu
                irinicio() {
                    while (this.rama.padre != null) {
                        this.rama = this.rama.padre;
                    }
                }

                //Metodo que permite moverse al menu padre
                irapadre() {
                    if (this.rama.padre != null) {
                        this.rama = this.rama.padre;
                        return true; //Si se pudo mover al padre devuelve true
                    } else {
                        return false; //Si no se pudo devuelve false
                    }
                }

                //Metodo que busca el menu hacia la raiz solamente
                iramenu(nommenu) {
                    if (this.rama.nodo == null) {
                        return false;
                    }
                    while (this.rama.nodo != null && this.rama.nodo.opcion != nommenu) {
                        if (!this.irapadre()) {
                            return;
                        }
                    }
                }
            }





    });