angular.module("webapp")
    .controller('ModaInstanceLoginCtrl', function ($scope, $uibModalInstance, data, Notificaciones, Serviciodebug, navegacion, PeticionHttp, Auth, $rootScope, $location) {
        //DEBUG
        Serviciodebug.bandera = true;
        // END DEBUG

        var pc = this;
        pc.titulo = "Login";
        pc.data = data;
        $scope.login = {};
        $scope.request = {};

        $scope.botonlogin = {
            color: 'primary',
            tamano: 'small',
            texto: 'Entrar',
            cargando_boton: false,
            icono:'fa-mail-forward',
            datos:{}
        };
        $scope.txtUser ={
            txt:{
                placeholder: 'Usuario',
                label: 'Usuario',
                icono:'fa-user',
            },
            boton:{
                encendido:false,
                iconoBoton:'fa-save',
                color:'default',
                cargandoBoton: false,
            },
            datos:{}
        };
        $scope.txtcontrasena ={
            txt:{
                placeholder: 'Contraseña',
                label: 'Contraseña',
                icono:'fa-lock',
                password: true
            },
            boton:{
                encendido:false,
                iconoBoton:'fa-save',
                color:'default',
                cargandoBoton: false,
            },
            datos:{}
        };

        pc.ok = function () {
            $scope.botonlogin.cargando_boton= true;
            $scope.request.tipo = "login";
            $scope.request.cmdj = "usuario = '" + $scope.login.username + "' and password = '" + $scope.login.contrasena + "'";

            PeticionHttp.getAll($scope.request)
                .then(function (data) {
                    if(Auth.login(data.rows[0])){
                        navegacion.cargarMenu(true);
                        $uibModalInstance.close();
                        $scope.botonlogin.cargando_boton = false;
                    }
                }, function(){
                    $scope.botonlogin.cargando_boton= false;
                });
        };
        pc.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }
    });