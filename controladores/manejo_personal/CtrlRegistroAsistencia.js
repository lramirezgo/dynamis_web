angular.module('webapp')
    .controller('CtrlRegistroAsistencia', function ($scope, msgBox, PeticionHttp, $location, pasoInfo, Notificaciones) {
        $scope.asistencia = {};
        $scope.request = {};
        $scope.foco_serie = true;

        //CONFIGURACIONES
        $scope.txtregistro = {
            txt: {
                placeholder: 'Numero de empleado/ usuario',
                label: 'Numero de empleado/ usuario',
                icono: ' fa-barcode',
                password: false,
                disabled: false,
                foco: true,
                func: function () {
                    $scope.registrar_asistencia();
                }
            },
            boton: {
                encendido: false,
                iconoBoton: 'fa-eraser',
                color: $scope.color_elementos,
                cargandoBoton: false,
            },
            datos: {}
        };
        $scope.tabla = {
            nocolapsado: [
                {
                    alias: 'NOMBRE',
                    campo: 'nombre',
                    tamano: '4'
                },
                {
                    alias: 'FECHA HORA',
                    campo: 'fechahora',
                    tamano: '4'
                },
                {
                    alias: 'MOVIMIENTO',
                    campo: 'movimiento_desc',
                    tamano: '4'
                }
            ],
            colapsado: ['nombre', 'fechahora', 'movimiento_desc'],
            colapsarsiempre: false,
            func: function (registro) {
            }
        };
        //CONFIGURACIONES

        $scope.registrar_asistencia = function () {
            $scope.cargando = true;
            $scope.request.cmdj = "registrar asistencia where usuario = '" + $scope.asistencia.num_empleado + "'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_reg_asistencia) {
                    Notificaciones.notificacion("CORRECTO", data_reg_asistencia.rows[0].msj, "success", true);
                    $scope.cargando = false;
                    $scope.asistencia.num_empleado = '';
                    $scope.lista_ultimas_asistencias();
                    $scope.ultimo_registro();
                }, function () {
                    $scope.cargando = false;
                    $scope.asistencia.num_empleado = '';
                });
        };
        $scope.lista_ultimas_asistencias = function () {
            $scope.request = {};
            $scope.cargando_lista = true;
            $scope.request.cmdj = "listar asistencia detalle where limit = '10'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_lista_asistencia) {
                    $scope.datos_lista = data_lista_asistencia.rows;
                    $scope.cargando_lista = false;

                }, function () {
                    $scope.cargando_lista = false;
                });
        };
        $scope.ultimo_registro = function () {
            $scope.request = {};
            $scope.cargando = true;
            $scope.request.cmdj = "listar asistencia detalle where limit = '1'";
            PeticionHttp.getAll($scope.request)
                .then(function (data_ultimo_registro) {
                    $scope.datos_ultimo = data_ultimo_registro.rows;
                    $scope.movimiento_desc = data_ultimo_registro.rows[0].movimiento_desc
                    $scope.cargando = false;

                }, function () {
                    $scope.cargando = false;
                });
        };

        $scope.lista_ultimas_asistencias();
        $scope.ultimo_registro();
    });