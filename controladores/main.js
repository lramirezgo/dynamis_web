angular.module("webapp")
    .controller('main', function ($scope, msgBox, Auth, Serviciodebug, navegacion, $cookies, $location, $window) {
        $scope.mensaje = "Inicia aplicacion";
        $scope.titulo = "Dynamis"
        $scope.titulo1 = "CLI"
        $scope.titulo = "Dynamis WEB"
        $scope.titulo1 = "CLI"
        $scope.titulo = "Dynamis"
        $scope.titulo1 = "WEB"

        $scope.titulo_mini = "D"
        $scope.titulo_mini1 = "W"

        $scope.titulo_aplicacion = "Dynamis WEB"

        $scope.color_tema = 'skin-blue';
        $scope.color_elementos = 'primary';
        //DEBUG
        Serviciodebug.bandera = false;
        // END DEBUG
        $scope.usuario = $cookies.get('usuario');

        if (!Auth.hay_sesion()) {
            msgBox.login();
        } else {
            navegacion.cargarMenu();
        };

        $scope.cerrar_sesion = function () {
            Auth.logout();
            $window.location.reload();
        };

        $scope.abrir_trace = function(){
             msgBox.popUpOk({mensaje:"",titulo:"Rastreo de servidor", template:"views/trace/msjBoxTrace.html" , controlador:"CtrlTrace"});
        };


    });