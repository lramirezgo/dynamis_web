angular.module("webapp", ['ngRoute', 'ngAnimate', 'ngMaterialDatePicker', 'ui.bootstrap', 'Serviciodebug', 'Notificaciones', 'PeticionHttp', 'ngCookies', 'Auth','ngMaterial', 'ngFileUpload', 'exportar','chart.js' ])
    .config(function ($provide, $routeProvider) {
        //Esto permite usar el routeProvider en el los controladores de la aplicacion para cargar las rutas dinamicamente
        $provide.factory('$routeProvider', function () {
            return $routeProvider;
        });

    })
    .run(function (navegacion, msgBox, Auth) {
        //Esta parte se debe quitar pues, las rutas se cargaran despues de iniciar sesion o si ya esta la sesion abierta
        /*sesion = false;
        if(sesion){
            navegacion.cargarMenu();
        }else{
            msgBox.login();
        }*/

       /* if (Auth.sesion == '' || Auth.sesion == undefined) {
            msgBox.login();
        } else {
            navegacion.cargarMenu();
        }*/

    }) 
