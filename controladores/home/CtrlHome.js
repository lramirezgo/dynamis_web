angular.module('webapp')
    .controller('CtrlHome', function ($scope, msgBox, PeticionHttp) {
        $scope.request = {};
        $scope.titulo = "Panel de control";
        $scope.ampliar_totales = true;
        $scope.tamano_totales = 6;
        $scope.grafica_pastel = true;
        $scope.tamano_monetario = 6;
        $scope.ampliar_monetario = true;
        $scope.grafica_pastel_monetario = true;
        $scope.cargando = false;
        $scope.options = {
            legend: {
                display: true
            }
        };

       

        $scope.cuenta_remisiones = function () {
            $scope.request.tipo = "request"
            $scope.request.cmdj = "listar cuenta remisiones por tipo";
            PeticionHttp.getAll($scope.request)
                .then(function (data_cuenta_rem) {
                    $scope.datos_cuenta_rem = data_cuenta_rem.rows;
                    $scope.total_remisiones = $scope.datos_cuenta_rem[0].total_rem;
                    $scope.sum_rem = $scope.datos_cuenta_rem[0].sum_rem;
                    ///GRAFICA DE DONA
                    $scope.data = [];
                    $scope.data_por_mon = [];
                    $scope.labels = [];

                    $scope.labels1 = ["Download Sales", "In-Store Sales", "Mail-Order Sales", "Tele Sales", "Corporate Sales"];
                    $scope.data1 = [300, 500, 100, 40, 120];

                    for (var i = 0; i < $scope.datos_cuenta_rem.length; i++) {

                        $scope.labels.push($scope.datos_cuenta_rem[i].descripcion);
                        $scope.data.push($scope.datos_cuenta_rem[i].por_total_rem);
                        $scope.data_por_mon.push($scope.datos_cuenta_rem[i].sum_rem_por);



                    };


                });
        };
        $scope.top_mas_vendidos = function () {
            $scope.cargando = true;
            $scope.request = {};
            $scope.request.tipo = "request"
            $scope.request.cmdj = "listar top vendidos";
            PeticionHttp.getAll($scope.request)
                .then(function (data_top_mas) {
                    $scope.top_mas = data_top_mas.rows;
                    $scope.cargando = false;
                });
        };
        $scope.top_menos_vendidos = function () {
            $scope.cargando = true;
            $scope.request = {};
            $scope.request.tipo = "request"
            $scope.request.cmdj = "listar top menos vendidos";
            PeticionHttp.getAll($scope.request)
                .then(function (data_top_menos) {
                    $scope.top_menos = data_top_menos.rows;
                    $scope.cargando = false;
                });
        };

        /// ACCIONES DE GRAFICAS
        $scope.ampliar_grafica_totales = function () {
            $scope.tamano_totales = '12';
            $scope.ampliar_totales = false;
        };
        $scope.minimizar_grafica_totales = function () {
            $scope.tamano_totales = '6';
            $scope.ampliar_totales = true;
        };
        $scope.ver_grafica_barras = function () {
            $scope.grafica_pastel = false;
        };
        $scope.ver_grafica_pastel = function () {
            $scope.grafica_pastel = true;
        };

        $scope.ampliar_grafica_monetario = function () {
            $scope.tamano_monetario = '12';
            $scope.ampliar_monetario = false;
        };
        $scope.minimizar_grafica_monetario = function () {
            $scope.tamano_monetario = '6';
            $scope.ampliar_monetario = true;
        };
        $scope.ver_grafica_barras_monetario = function () {
            $scope.grafica_pastel_monetario = false;
        };
        $scope.ver_grafica_pastel_monetario = function () {
            $scope.grafica_pastel_monetario = true;
        };
        ///TERMINAN ACCIONES DE  GRAFICAS


        $scope.cuenta_remisiones();
        $scope.top_mas_vendidos();
        $scope.top_menos_vendidos();
    });