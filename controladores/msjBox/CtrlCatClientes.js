angular.module("webapp")
    .controller('CtrlCatClientes', function ($scope, msgBox, data, $uibModalInstance, PeticionHttp, data, $rootScope) {
        $scope.request = {};
        $scope.titulo = 'Catalogo clientes';
        var pc = this;
        pc.data = data;
        pc.respuesta = null;
        $scope.loadingText = 'Buscando';
        var tipo_cliente;
        if(pc.data.datos == '' || pc.data.datos == undefined){
            tipo_cliente = ""
        }else{
            tipo_cliente = " where tipocliente = '" + pc.data.datos + "'";
        };
        pc.ok = function () {
            $uibModalInstance.close("ok");
        }

        pc.cancel = function () {
            $uibModalInstance.dismiss();
        }

        $scope.tabla = {
            nocolapsado: [],
            colapsado: ['codigo_cliente', 'nombre', 'direccion'],
            func: function (registro) {
                pc.respuesta = registro;
                $uibModalInstance.close(pc.respuesta);
            },
            colapsarsiempre: true
        };


        $scope.lista = function () {
            $scope.isRouteLoading = true;
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar clientes" + tipo_cliente +"";
            PeticionHttp.getAll($scope.request)
                .then(function (data) {
                    $scope.datos_lista = data.rows;
                    $scope.num_registros = $scope.datos_lista.length;
                    $scope.num_paginas = (Math.ceil($scope.datos_lista.length / 10));
                    $scope.isRouteLoading = false;

                });
        };

        $scope.seleccionar = function (registro) {
            pc.respuesta = registro;
            $uibModalInstance.close(pc.respuesta);
        }

        $scope.lista();


    })