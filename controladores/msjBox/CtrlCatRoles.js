angular.module('webapp')
    .controller('CtrlCatRoles', function ($scope, data, $uibModalInstance, PeticionHttp, data) {
        $scope.titulo = 'Catalogo roles';
        var pc = this;
        pc.data = data;
        pc.respuesta = null;
        $scope.loadingText = 'Buscando';
        pc.ok = function () {
            $uibModalInstance.close();
            //Cargo las rutas dinamicamente solo si se inicio sesion    
            //navegacion.cargarMenu();
        };
        pc.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.tabla = {
            nocolapsado: [
                {
                    alias: 'ROL',
                    campo: 'rol',
                    tamano: '6'
                },
                {
                    alias: 'DESCRIPCION',
                    campo: 'descripcion',
                    tamano: '6'
                }
            ],
            colapsado: ['rol', 'descripcion'],
            colapsarsiempre: true,
            func: function(registro){
                pc.respuesta=registro;
                $uibModalInstance.close(pc.respuesta);
            }
        };
      
        $scope.lista_roles = function () {
            $scope.isRouteLoading = true;
            $scope.request = {};
            $scope.cargando = true;
            $scope.request.cmdj = "listar roles";
            PeticionHttp.getAll($scope.request)
                .then(function (data) {
                    $scope.lista_roles = data.rows;
                    $scope.cargando = false;
                    $scope.isRouteLoading = false;
                });
        };
      
        $scope.lista_roles();


    });