angular.module("webapp")
    .controller('CtrlCatProductos', function ($scope, msgBox, data, $uibModalInstance, PeticionHttp, data, $rootScope) {
        $scope.titulo = 'Catalogo de productos';
        $scope.request = {};
        var pc = this;
        pc.data = data;
        pc.respuesta = null;
        $scope.loadingText = 'Buscando';
        pc.ok = function () {
            $uibModalInstance.close("ok");
        }

        pc.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }

        if(pc.data.datos == '' || pc.data. datos == undefined){
            $wh = '';
        }else{
            $wh = " where tiporem = '" + data.datos + "'";

        }
        $scope.tabla = {
            nocolapsado: [
                {
                    alias: 'CODIGO',
                    campo: 'codigo',
                    tamano: '2'
                },
                {
                    alias: 'CODIGO ALTERNO',
                    campo: 'mostrarcodigo',
                    tamano: '2'
                },
                {
                    alias: 'DESCRIPCION',
                    campo: 'descripcion',
                    tamano: '2'
                },
                {
                    alias: 'FAMILIA',
                    campo: 'familia',
                    tamano: '2'
                },
                {
                    alias: 'TIPO',
                    campo: 'tipo',
                    tamano: '2'
                },
                {
                    alias: 'PRECIO',
                    campo: 'precio',
                    tamano: '1'
                },
                {
                    alias: 'TIPO PRECIO',
                    campo: 'tipoprecio',
                    tamano: '1'
                }
            ],
            colapsado: ['codigo', 'mostrarcodigo', 'descripcion', 'familia'],
            colapsarsiempre: true,
            func: function (registro) {
                pc.respuesta = registro;
            $uibModalInstance.close(pc.respuesta);
            }
        };
        $scope.lista = function () {
            $scope.isRouteLoading = true;
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar catalogo productos" + $wh + "";
            PeticionHttp.getAll($scope.request)
                .then(function (data) {
                    $scope.datos_lista = data.rows;
                    $scope.num_registros = $scope.datos_lista.length;
                    $scope.num_paginas = (Math.ceil($scope.datos_lista.length / 10));
                    $scope.isRouteLoading = false;

                });
        };

        $scope.lista();


    })