angular.module("webapp")
    .controller('CtrlCatalogos', function ($scope, msgBox, data, $uibModalInstance, PeticionHttp, data, $rootScope) {
        $scope.request = {};
        $scope.titulo = 'Catalogo';
        var pc = this;
        pc.data = data;
        pc.respuesta = null;
        $scope.loadingText = 'Buscando';
        var tipo_catalogo;

        if(pc.data.datos == '' || pc.data.datos == undefined){
            tipo_catalogo = "grp = ''"
        }
        if(pc.data.datos.grp != '' & pc.data.datos.subgrp != ''){
            tipo_catalogo = "grp = '" + pc.data.datos.grp + "' and subgrp = '" + pc.data.datos.subgrp + "' and estado = 'A'";
        };
        if(pc.data.datos.grp != '' & pc.data.datos.subgrp == undefined || pc.data.datos.grp != '' & pc.data.datos.subgrp == ''){
            tipo_catalogo = "grp = '" + pc.data.datos.grp + "'  and estado = 'A'";
        };

        $scope.tabla = {
            nocolapsado: [
            ],
            colapsado: ['codigo', 'descripcion'],
            func: function(registro){
                pc.respuesta=registro;
                $uibModalInstance.close(pc.respuesta);
            },
            colapsarsiempre: true
        };
        pc.ok = function () {
            $uibModalInstance.close("ok");
        };
        pc.cancel = function () {
            $uibModalInstance.dismiss();
        };
        $scope.lista = function () {
            $scope.isRouteLoading = true;
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar codigos  where " + tipo_catalogo + "";
            PeticionHttp.getAll($scope.request)
                .then(function (data) {
                    $scope.datos_lista = data.rows;
                    $scope.num_registros = $scope.datos_lista.length;
                    $scope.num_paginas = (Math.ceil($scope.datos_lista.length / 10));
                    $scope.isRouteLoading = false;

                });
        };
        $scope.seleccionar = function (registro) {
            pc.respuesta = registro;
            $uibModalInstance.close(pc.respuesta);
        };
        $scope.lista();


    })