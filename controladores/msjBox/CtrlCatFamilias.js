angular.module("webapp")
    .controller('CtrlCatFamilias', function ($scope, msgBox, data, $uibModalInstance, PeticionHttp, data, $rootScope) {
        $scope.request = {};
        $scope.titulo = 'Catalogo de familias';
        var pc = this;
        pc.data = data;
        pc.respuesta = null;
        $scope.loadingText = 'Buscando';
        var tipo_familia;
        if(pc.data.datos == '' || pc.data.datos == undefined){
            tipo_familia = ""
        }else{
            tipo_familia = "and subgrp = '" + pc.data.datos + "'";
        };
        $scope.tabla = {
            nocolapsado: [
            ],
            colapsado: ['codigo', 'subgrp', 'descripcion'],
            func: function(registro){
                pc.respuesta=registro;
                $uibModalInstance.close(pc.respuesta);
            },
            colapsarsiempre: true
        };
        pc.ok = function () {
            $uibModalInstance.close("ok");
        };
        pc.cancel = function () {
            $uibModalInstance.dismiss();
        };
        $scope.lista = function () {
            $scope.isRouteLoading = true;
            $scope.request.tipo = "request";
            $scope.request.cmdj = "listar codigos where grp='CVEFAMILIA' " + tipo_familia +" and estado = 'A'";
            PeticionHttp.getAll($scope.request)
                .then(function (data) {
                    $scope.datos_lista = data.rows;
                    $scope.num_registros = $scope.datos_lista.length;
                    $scope.num_paginas = (Math.ceil($scope.datos_lista.length / 10));
                    $scope.isRouteLoading = false;

                });
        };
        $scope.seleccionar = function (registro) {
            pc.respuesta = registro;
            $uibModalInstance.close(pc.respuesta);
        };
        $scope.lista();


    })