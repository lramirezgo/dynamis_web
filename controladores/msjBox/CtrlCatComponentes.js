angular.module("webapp")
    .controller('CtrlCatComponentes', function ($scope, msgBox, data, $uibModalInstance, PeticionHttp, data, $rootScope) {
        $scope.request = {};
        $scope.titulo = 'Catalogo componentes';
        var pc = this;
        pc.data = data;
        pc.respuesta = null;
        $scope.loadingText = 'Buscando';

        pc.ok = function () {
            $uibModalInstance.close("ok");
        }

        pc.cancel = function () {
            $uibModalInstance.dismiss();
        }

        $scope.tabla = {
            nocolapsado: [
                {
                    alias: 'CODIGO',
                    campo: 'codigo',
                    tamano: '2'
                },
                {
                    alias: 'COD. ALTERNO',
                    campo: 'mostrarcodigo',
                    tamano: '2'
                },
                {
                    alias: 'DESCRIPCION',
                    campo: 'descripcion',
                    tamano: '2'
                },
                {
                    alias: 'FAMILIA',
                    campo: 'familia',
                    tamano: '2'
                },
                {
                    alias: 'TIPO',
                    campo: 'tipo',
                    tamano: '2'
                },
                {
                    alias: 'UNIDAD',
                    campo: 'unidad',
                    tamano: '2'
                }
            ],
            colapsado: ['codigo', 'descripcion', 'familia'],
            func: function (registro) {
                pc.respuesta = registro;
                $uibModalInstance.close(pc.respuesta);
            },
            colapsarsiempre: true
        };

        $scope.lista = function () {
            $scope.isRouteLoading = true;
            $scope.request.cmdj = "listar componentes where estado = 'A'";
            PeticionHttp.getAll($scope.request)
                .then(function (data) {
                    $scope.datos_lista = data.rows;
                    $scope.isRouteLoading = false;

                });
        };


        $scope.lista();


    })