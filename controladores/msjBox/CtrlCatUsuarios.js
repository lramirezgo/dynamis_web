angular.module("webapp")
    .controller('CtrlCatUsuarios', function ($scope, msgBox, data, $uibModalInstance, PeticionHttp, data, $rootScope) {
        $scope.request = {};
        $scope.titulo = 'Catalogo de familias';
        var pc = this;
        pc.data = data;
        pc.respuesta = null;
        $scope.loadingText = 'Buscando';

        $scope.tabla = {
            nocolapsado: [
            ],
            colapsado: ['usuario', 'nombre', 'puesto'],
            func: function (registro) {
                pc.respuesta = registro;
                $uibModalInstance.close(pc.respuesta);
            },
            colapsarsiempre: true
        };
        pc.ok = function () {
            $uibModalInstance.close("ok");
        };

        pc.cancel = function () {
            $uibModalInstance.dismiss();
        };

        $scope.lista = function () {
            $scope.isRouteLoading = true;
            $scope.request.cmdj = "listar usuarios";
            PeticionHttp.getAll($scope.request)
                .then(function (data) {
                    $scope.datos_lista = data.rows;
                    $scope.num_registros = $scope.datos_lista.length;
                    $scope.num_paginas = (Math.ceil($scope.datos_lista.length / 10));
                    $scope.isRouteLoading = false;

                });
        };

        $scope.seleccionar = function (registro) {
            pc.respuesta = registro;
            $uibModalInstance.close(pc.respuesta);
        }

        $scope.lista();


    })