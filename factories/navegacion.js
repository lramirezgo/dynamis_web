angular.module('webapp')
    .factory('navegacion', function ($rootScope, $location, $routeProvider, Auth, PeticionHttp, varEntorno, $route) {

        navegacion = {
            menu: [],
            cargarMenu: function (irHome) {
                f = this;
                f.irHome = irHome;
                //Aqui se debe obtener la estructura del menu
                PeticionHttp.getAll(
                    {
                        cmdj: "listar menu navegacion where usuario = '" + Auth.usuario + "'"
                    }
                ).then(function (data_menu) {
                    menu = data_menu.rows;
                    $rootScope.$broadcast("crearMenu", data_menu.rows);
                    varEntorno.menu = data_menu;
                    varEntorno.cargarRutas(f);
                })
                //this.cargarRutas();
            },
            cargarRutas: function (f) {
                data_menu_rutas = varEntorno.menu;
                //RECORREMOS TODAS LAS PANTALLAS PARA DESPUES CREAR LAS RUTAS
                data_menu_rutas.rows.forEach(function (menu_elemento) {
                    if (menu_elemento.tipo == 'pantalla') {
                        menu_elemento.string1 = menu_elemento.string1;
                        var ruta = menu_elemento.string1.url.replace('#!', '/');

                        $routeProvider.when(ruta, {
                            templateUrl: menu_elemento.string1.template,
                            controller: menu_elemento.string2
                        });
                    }
                });
                if (f.irHome == true) {
                    f.irHome = false;
                    f.iraHome();
                } else {
                    $route.reload();
                }
            },
            iraHome: function () {
                data_menu_rutas = varEntorno.menu;
                for (i = 0; i < data_menu_rutas.rows.length; i++) {
                    if (data_menu_rutas.rows[i].tipo == 'pantalla') {
                        primer_pantalla = data_menu_rutas.rows[i].string1;
                        var ruta_inicio = primer_pantalla.url.replace('#!', '/');
                        $location.url(ruta_inicio);
                        $route.reload();
                        break;
                    }
                }
            }


        };

        varEntorno.cargarRutas = navegacion.cargarRutas;
        return navegacion;
    })