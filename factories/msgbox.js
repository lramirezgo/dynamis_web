angular.module('webapp')
    .factory('msgBox', function ($uibModal) {
        return {
            okCancel: function (a) {

                var modalInstance = $uibModal.open({
                    animation: true,
                    arialLabelledBy: 'modal-title',
                    ariaDescribeBy: 'modal-body',
                    templateUrl: 'factories/templates/msjBox/msjBoxOkCancel.html',
                    controller: 'ModaInstanceCtrl',
                    controllerAs: 'pc',
                    keyboard: false,
                    //backdrop:'static',
                    resolve: {
                        data: function () {
                            return a;
                        }
                    }
                });
                return modalInstance.result;
            },
            ok: function (a) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    arialLabelledBy: 'modal-title',
                    ariaDescribeBy: 'modal-body',
                    templateUrl: 'factories/templates/msjBox/msjBoxOk.html',
                    controller: 'ModaInstanceCtrl',
                    controllerAs: 'pc',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        data: function () {
                            return a;
                        }
                    }
                });
                return modalInstance.result;
            },
            login: function (a) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    arialLabelledBy: 'modal-title',
                    ariaDescribeBy: 'modal-body',
                    templateUrl: 'factories/templates/msjBox/msjLogin.html',
                    //template:'Esto esta bueno',
                    controller: 'ModaInstanceLoginCtrl',
                    windowClass: 'window', //esta clase sirve para dar color al fondo, se utiliza para el login
                    controllerAs: 'pc',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        data: function () {
                            return a;
                        }
                    }
                });
                return modalInstance.result;
            },
            popUpOk: function (a) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    arialLabelledBy: 'modal-title',
                    ariaDescribeBy: 'modal-body',
                    templateUrl: a.template,
                    controller: a.controlador,
                    controllerAs: 'pc',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        data: function () {
                            return a;
                        }
                    }
                });
                return modalInstance.result;
            },
        }
    })
    .controller('ModaInstanceCtrl', function ($uibModalInstance, data, navegacion) {
        var pc = this;
        pc.data = data;

        pc.ok = function () {
            $uibModalInstance.close("ok");
        }

        pc.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }


    });