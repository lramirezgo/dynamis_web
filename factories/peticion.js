angular.module("PeticionHttp", []).
    factory('PeticionHttp', function ($http, $q, Notificaciones) {
        return {
            getAll: getAll,
            trace: '', 
            archivo:'',
            rastreo:''
        }
        function getAll(datos) {
            datos.archivo = this.archivo;
            datos.rastreo = this.rastreo;
            
            var defered = $q.defer();
            var promesa = defered.promise;
            $http({
                method: "POST",

                url: 'http://localhost/dynamis/',
                //url: 'http://192.168.1.3/dynamis/', 
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: datos

            }).then(function (inyectorResponse) {
                if (inyectorResponse.data.serverError == true) {
                    Notificaciones.banderaNotificacion = true;
                    Notificaciones.notificacion(inyectorResponse.data.rows[0].msj, "", "error", true);
                    defered.reject();
                } else {
                    defered.resolve(inyectorResponse.data, false);
                }
            });

            return promesa;
        };

    });