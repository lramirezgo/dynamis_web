angular.module('webapp')
.factory('dynamjs',function($http){
    return {
        peticion:function(cookoff){
            return $http({
                method: "POST",
                //url: "http://localhost:3000/peticiones/crearconteo.json"
                url:"http://localhost:3500/api/dynamis",
                data:cookoff,
                headers: {
                    'Content-Type':undefined //Esto es necesario par enviar archivos, el navegador determina el tipo de contenido en base a los datos enviados
                },
                transformRequest:function(data){ //Esta funcion transforma la peticion para que soporte el envio de archivos y json
                    var fd = new FormData();                        
                    for(key in data){
                        if(key == "parametros"){
                            fd.append(key, angular.toJson(data.parametros));
                        }else{
                            fd.append(key,data[key]);
                        }
                    }
                    return fd
                }
            })
        }
    }
})
.factory('apiKeyInjector',function($q, Auth){
    var apiKeyInjector = {
        request:function(config){
            if(config.method == 'POST'){
                config.requestTimestamp = new Date().getTime();
                config.data.sesion = (Auth.sesion == undefined) ? '': Auth.sesion
                config.data.usuario = (Auth.usuario == undefined) ? '': Auth.usuario
                config.data.tipo = config.data.tipo == undefined ? 'request': config.data.tipo
                //config.data.archivo = config.data.archivo == undefined ? '': config.data.archivo
                //config.data.rastreo = config.data.rastreo == undefined ? false: config.data.rastreo
                //console.log(config.data);
            }         

            //config.params.apiKey = 'esto-es-la-llave'
            return config;
        },
        response:function(res){
            if(res.config.url.indexOf('/dynamis') !== -1){                   
                time = new Date().getTime() - res.config.requestTimestamp;
                //console.log(res.config.data.accion + " ("+ time / 1000 + " seconds)");
                /*if(res.data.error){
                    return $q.reject(res.data.info);
                }else{
                    return res.data.info;
                } */   
                if (res.status == 200) {
                    $q.resolve(res.data);
                    //console.info("si conecto");
                } else {
                    //console.info(res);
                    //console.info("no conecto");
                }                
            }
            
            return res;
        },
        responseError: function (response) {
            return $q.reject(response.data);
        }

    }
    return apiKeyInjector;
})
.config(function ($httpProvider) {
    $httpProvider.interceptors.push('apiKeyInjector');
});