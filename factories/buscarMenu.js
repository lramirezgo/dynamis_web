angular.module('webapp')
    .factory('buscarMenu', function ($rootScope, $routeProvider) {
        return {
            menu1: [],
            cargarMenu: function () {
                //Aqui se debe obtener la estructura del menu

                this.menu1 = [
                    { url: "#!important", text: "Important", icon: "fa-circle-o text-red" },
                    { url: "#!warning", text: "Warning", icon: "fa-circle-o text-yellow" },
                    { url: "#!information", text: "Information", icon: "fa-circle-o text-aqua" },
                    { url: "#!dinamico", text: "Dinamico", icon: "fa-circle-o text-aqua" }
                ];
                this.cargarRutas();
                $rootScope.$broadcast("crearMenu");
            },
            cargarRutas: function () {

                //Aqui se debe cargar las rutas desde la BD
                $routeProvider
                    .when('/', {
                        templateUrl: 'views/home.html',
                        controller: 'test'
                        /* //Esta propiedad sirve para que se de un retrazo al cargar las views, sirve para probar el loading
                        ,resolve: {
                            // I will cause a 1 second delay
                            delay: function($q, $timeout) {
                                var delay = $q.defer();
                                $timeout(delay.resolve, 1000);
                                return delay.promise;
                            }
                            }
                        */
                    })
                    .when('/important', {
                        templateUrl: 'views/important.html',
                        controller: 'test'
                    })
                    .when('/warning', {
                        templateUrl: 'views/warning.html',
                        controller: 'test'
                    })
                    .when('/information', {
                        templateUrl: 'views/information.html',
                        controller: 'test'
                    })
                    .when('/dinamico', {
                        templateUrl: 'views/important.html',
                        controller: 'test'
                    });

            }

        }
    })