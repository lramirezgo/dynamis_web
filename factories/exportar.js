angular.module("exportar", []).
factory('exportar', function() {
     
    convertArrayOfObjectsToCSV  = function(args){  
        var result, ctr, keys, columnDelimiter, lineDelimiter, data;

        data = args.data || null;
        if (data == null || !data.length) {
            return null;
        }

        columnDelimiter = args.columnDelimiter || ',';
        lineDelimiter = args.lineDelimiter || '\n';

    keys = Object.keys(data[0]);
    keys.pop();		

        result = '';
        result += keys.join(columnDelimiter);
        result += lineDelimiter;

        data.forEach(function(item) {
            ctr = 0;
            keys.forEach(function(key) {
                if (ctr > 0) result += columnDelimiter;

                result += item[key];
                ctr++;
            });
            result += lineDelimiter;
    });
    return result;
  }

  downloadCSV =function(args){
    var data, filename, link;
        var csv = convertArrayOfObjectsToCSV({
            data: args.dataarray
        });
        if (csv == null) return;

        filename = args.filename || 'export.csv';

        if (!csv.match(/^data:text\/csv/i)) {
            csv = 'data:text/csv;charset=utf-8,' + csv;
        }
        data = encodeURI(csv);

        link = document.createElement('a');
        link.setAttribute('href', data);
        link.setAttribute('download', filename);
        link.click();
  }

  exportExcel = function(args){

    //Funcion para obtener las keys de un json
    function keys(o) {
      var ks = Object.keys(o), o2 = [];
      for(var i = 0; i < ks.length; ++i) if(o.hasOwnProperty(ks[i])) o2.push(ks[i]);
      return o2;
    }
    
    /*data = [
      ["#", "Movie", "Actor"],
      ["1", "Fight Club", "Brad Pitt"],
      ["2", "Matrix (Series)", "Keanu Reeves"],
      ["3", "V for Vendetta", "Hugo Weaving"]
    ];*/    

    data2 = [
      {"columna1":"fila 1 Columa A","columna2":"fila 1 Columna B"},
      {"columna1":"fila 2 Columa A","columna2":"fila 2 Columna B"},
      {"columna1":"fila 3 Columa A","columna2":"fila 3 Columna B"}
    ];		
  
    function datenum(v, date1904) {
      if(date1904) v+=1462;
      var epoch = Date.parse(v);
      return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
    };
    
    function getSheetOri(data, opts) {
      var ws = {};
      var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
      for(var R = 0; R != data.length; ++R) {
        for(var C = 0; C != data[R].length; ++C) {
          if(range.s.r > R) range.s.r = R;
          if(range.s.c > C) range.s.c = C;
          if(range.e.r < R) range.e.r = R;
          if(range.e.c < C) range.e.c = C;
          var cell = {v: data[R][C] };
          if(cell.v == null) continue;
          var cell_ref = XLSX.utils.encode_cell({c:C,r:R});
          
          if(typeof cell.v === 'number') cell.t = 'n';
          else if(typeof cell.v === 'boolean') cell.t = 'b';
          else if(cell.v instanceof Date) {
            cell.t = 'n'; cell.z = XLSX.SSF._table[14];
            cell.v = datenum(cell.v);
          }
          else cell.t = 's';
          
          ws[cell_ref] = cell;
        }
      }
      if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
      return ws;
    };
    
    function getSheet(data2, opts) {			

      var ws = {};
      var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
      /*for(var R = 0; R != data.length; ++R) {
        for(var C = 0; C != data[R].length; ++C) {
          if(range.s.r > R) range.s.r = R;
          if(range.s.c > C) range.s.c = C;
          if(range.e.r < R) range.e.r = R;
          if(range.e.c < C) range.e.c = C;
          var cell = {v: data[R][C] };
          if(cell.v == null) continue;
          var cell_ref = XLSX.utils.encode_cell({c:C,r:R});
          
          if(typeof cell.v === 'number') cell.t = 'n';
          else if(typeof cell.v === 'boolean') cell.t = 'b';
          else if(cell.v instanceof Date) {
            cell.t = 'n'; cell.z = XLSX.SSF._table[14];
            cell.v = datenum(cell.v);
          }
          else cell.t = 's';
          
          ws[cell_ref] = cell;
        }
      }*/

      //Para ver las opciones de estas clases ver el archivo xlsx.js
      objarray = data2;
      objarray.forEach(function(row, R){				
        
        columnas = keys(row);
        if(columnas[columnas.length-1]=="$$hashKey"){columnas.pop();}//Quito la ultima columna porque da el hashkey				
        columnas.forEach(function(columna,C){

          if(range.s.r > R) range.s.r = R;
          if(range.s.c > C) range.s.c = C;
          if(range.e.r < R) range.e.r = R;
          if(range.e.c < C) range.e.c = C;

          if(R == 0){
            encabezadocell = {v: columna};
            encabezadocell.t = 's';
            var cell_ref = XLSX.utils.encode_cell({c:C,r:R});	
            ws[cell_ref] = encabezadocell;					
          }

          var cell = {v: row[columna]};
          if(cell.v == null) cell.v = "";					
          
          var cell_ref = XLSX.utils.encode_cell({c:C,r:R+1});

          if(typeof cell.v === 'number') cell.t = 'n';
          else if(typeof cell.v === 'boolean') cell.t = 'b';
          else if(cell.v instanceof Date) {
            cell.t = 'n'; cell.z = XLSX.SSF._table[14];
            cell.v = datenum(cell.v);
          }
          else cell.t = 's';
          
          ws[cell_ref] = cell;									
          /*console.info(R + " " + C);
          console.info(columna);
          console.info(row[columna]);*/
        });
      });

      range.e.r +=1; //Esto es para aumentar en uno el filas de salida del rango de lo contrario no muestra el ultimo reistro

      /*data2.forEach(function (JS,R) {
        
        keys(JS).forEach(function(k) {
          if((C=hdr.indexOf(k)) == -1) hdr[C=hdr.length] = k;
          var v = JS[k]; //valor
          var t = 'z'; //formato
          var z = "";	//
          if(typeof v == 'number') t = 'n';
          else if(typeof v == 'boolean') t = 'b';
          else if(typeof v == 'string') t = 's';
          else if(v instanceof Date) {
            t = 'd';
            if(!o.cellDates) { t = 'n'; v = datenum(v); }
            z = o.dateNF || SSF._table[14];
          }
          ws[encode_cell({c:_C + C,r:_R + R + offset})] = cell = ({t:t, v:v});
          if(z) cell.z = z;
        });
      });*/


      if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
      return ws;
    };

    function Workbook() {
      if(!(this instanceof Workbook)) return new Workbook();
      this.SheetNames = [];
      this.Sheets = {};
    }
    
    var wb = new Workbook(), ws = getSheet(args.dataarray);
    /* add worksheet to workbook */
    wb.SheetNames.push(args.filename);
    wb.Sheets[args.filename] = ws;
    var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});
  
    function s2ab(s) {
      var buf = new ArrayBuffer(s.length);
      var view = new Uint8Array(buf);
      for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }
    
    saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), args.filename+'.xlsx');
    

  }

  var exportarData = {
    exportarCSV: function(filename,dataarray){
      downloadCSV({filename,dataarray});
    },
    exportarXLS: function(filename,dataarray){
      exportExcel({filename,dataarray});
    }		
  }
  return exportarData;
 
});