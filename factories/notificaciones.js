angular.module("Notificaciones", []).
factory('Notificaciones', function() {
     
    var variable= {
        banderaNotificacion: false,
        notificacion: function(title1, text1, type1, hide1){
            if(this.banderaNotificacion){
                new PNotify({
                    title: title1,
                    text: text1,
                    type: type1,
                    hide: hide1,
                    styling: 'bootstrap3'
                }); 
            }
            
        }
    };

   

    return variable;
 
});