angular.module("Auth", []).
factory('Auth', function($cookies ,$location ) {
        var variable = {
            sesion: '',
            usuario:'',
            sesion_abierta: false,
            login: function (datos_login) {
                $cookies.put('sesion', datos_login.sesion);
                $cookies.put('usuario', datos_login.usuario);
                $cookies.put('sesion_abierta', true);
                
                this.sesion = datos_login.sesion;
                this.usuario = datos_login.usuario;
                this.sesion_abierta = true;
                return true;
            },
            logout: function () {
                //al hacer logout eliminamos la cookie con $cookieStore.remove
                $cookies.remove("sesion");
                $cookies.remove("usuario");
                $cookies.remove("sesion_abierta");
            },
            hay_sesion: function(){
                if($cookies.get('sesion_abierta') == undefined){
                    return false;
                }else{
                    this.sesion =$cookies.get('sesion');
                    this.usuario =$cookies.get('usuario');
                    this.sesion_abierta =$cookies.get('sesion_abierta');
                    return true;
                }
                
            }
        };

        return variable;

    });